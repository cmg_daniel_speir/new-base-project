(function(){
	var ebrochure = {
		dom: function() {
			this.$ebrochureForm      = $('#ebrochure');
			this.$ebrochureError     = $('#ebrochure-error');
			this.$ebrochureRow       = $('[data-js-hook="ebrochure-row"]');
			this.$ebrochureEmailForm = $('#ebrochure-email');
			this.$ebrochureConfirm   = $('#formConfirm');
			this.$checkbox           = $('[data-js-hook="checkbox"]');
		},

		validateSelection: function() {
			var self = this;
			this.$ebrochureForm.submit(function(e){
				var checked = [];

				self.$checkbox.each(function(){
					if ($(this).prop('checked')) {
						checked.push(1);
					}
				});
				
				if (checked.length) {
					self.$ebrochureError.hide();
				} else {
					e.preventDefault();
					self.$ebrochureError.show();
				}
			});
		},

		emailForm: function() {
			this.$ebrochureEmailForm.formulate({
				require: 'brochureEmail{email}',
				errorClass: 'ebrochure__input--error',
				inlineMsg: true,
				inlineMsgClass: 'ebrochure__inline-msg',
				ajax: false
			});
		},

		rowSelect: function() {
			this.$checkbox.click(function(e){
				e.stopPropagation();
			});

			this.$ebrochureRow.click(function(e){
				var $checkbox = $('#' + $(this).data('checkbox'));
				if ($checkbox.prop('checked')) {
					$checkbox.prop('checked', false);
				} else {
					$checkbox.prop('checked', true);
				}
			});
		},

		ready: function() {
			this.dom();
			this.validateSelection();
			this.emailForm();
			this.rowSelect();
		}
	};

	$(function(){
		ebrochure.ready();
	});
})();