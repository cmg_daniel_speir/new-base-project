$(document).ready(function() {
  setupShareThis();
});

function setupShareThis(){
  var shareContainer = $('#share-this-popup');
  var shareLink = $('#share-link');
  var shareLoader = $('#share-loader');
  var activeClass = 'share-this-popup--show';

  shareContainer.data('loaded', '0');

  shareLink.on('click',function(e){
    e.preventDefault();
    e.stopPropagation();

    //If we don't have any Share This content on the page already, fetch it via AJAX
    if (shareContainer.data('loaded') == '0') {
      //indicate that it's loading...
      shareLoader.show();
      shareLink.hide();

      //mark Share This as loaded so we won't load it via AJAX again unless page is refreshed
      shareContainer.data('loaded', '1');

      //If user overrode defaults in the HTML markup, use the override values here
      var sUrl = shareContainer.data('url') || '';
      var sTweetFilter = shareContainer.data('tweet-filter') || 'GLOBAL';

      //fetch the Share This content via AJAX
      shareContainer.load(shareContainer.data('script'), { 'url': sUrl, 'tweet_filter': sTweetFilter }, function() {
        shareContainer.addClass(activeClass);
        //remove the loading indicator
        shareLoader.hide();
        shareLink.show();
      });
    }
    else {
      //We've already fetched Share This content for this page, so just show it
      shareContainer.toggleClass(activeClass);
    }
  });

  $('body').click(function() {
    //if the "SHARE" tooltip is visible, then close it
    if (shareContainer.hasClass(activeClass)) {
      shareContainer.toggleClass(activeClass);
    }
  });
}