(function(){
	var amenities = {
		dom: function() {
			this.$loadAll 	= $('#load-all-amenity');
			this.$loadLocal = $('[data-js-hook="load-local-amenity"]');
			this.$amenity   = $('[data-js-hook="amenity-hidden"]');
		},

		loadAll: function() {
			var self = this;
			this.$loadAll.click(function(e){
				e.preventDefault();
				$(this).slideUp(100);
				self.$loadLocal.hide();
				self.$amenity.slideDown(200);
			});
		},

		loadLocal: function() {
			var self = this;
			this.$loadLocal.click(function(e){
				e.preventDefault();
				$(this).slideUp(100);
				$('[data-amenity-group="' + $(this).attr('data-trigger-amenity-group') + '"]').slideDown(200);
			});
		},

		ready: function() {
			this.dom();
			this.loadAll();
			this.loadLocal();
		}
	}

	$(function(){
		amenities.ready();
	});
})();