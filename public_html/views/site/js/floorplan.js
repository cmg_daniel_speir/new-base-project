(function(){
  var floorplan = {
    dom: function() {
      this.$floorToggle            = $('[data-js-hook="floor-toggle"]');
      this.$dimensionToggle        = $('[data-js-hook="dimension-toggle"]');
      this.$scheduleTour           = $('#schedule-tour-toggle');
      this.$scheduleTourWrap       = $('#schedule-tour-wrap');
      this.$checkAvailability      = $('#check-availability');
      this.$checkAvailabilityTable = $('#check-availability-table');
      this.$leaseTermToggle        = $('[data-js-hook="toggle-lease-term"]');
      this.$datepicker             = $('#MoveInDate');
    },

    definitions: function() {
      this.activeButtonClass = 'floorplan-details__button--active';
      this.activeToggleClass = 'floorplan-details__toggle-link--active';
      this.visibleClass      = 'floorplan-details__image--visible';
    },

    datepicker: function() {
      // var picker = this.$datepicker.pickadate().data('pickadate');
    },

    dimensionToggle: function() {
      var self = this;

      this.$dimensionToggle.click(function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        var altId = id === '2d' ? '3d' : '2d';
        self.$dimensionToggle.removeClass(self.activeToggleClass);
        self.$floorToggle.removeClass(self.activeToggleClass);
        $(this).addClass(self.activeToggleClass);
        $('#' + altId + '-wrap').hide();
        $('#' + altId + '-toggle').hide();
        $('#' + altId + '-wrap').find('img').removeClass(self.visibleClass);
        $('#' + id + '-wrap').show().find('img').first().addClass(self.visibleClass);
        $('#' + id + '-toggle').show().find('li').first().find('a').addClass(self.activeToggleClass);
      });
    },

    floorToggle: function() {
      var self = this;
      this.$floorToggle.click(function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        var floor = id.split('-')[1];
        var dimenson = id.split('-')[0];
        self.$floorToggle.removeClass(self.activeToggleClass);
        $(this).addClass(self.activeToggleClass);
        $('#' + dimenson + '-wrap').find('img').removeClass(self.visibleClass);
        $('#' + dimenson + '-' + floor + '-floor').addClass(self.visibleClass);
      });
    },

    scheduleTour: function() {
      var self = this;
      this.$scheduleTour.click(function(e){
        e.preventDefault();
        self.$checkAvailability.removeClass(self.activeButtonClass);
        $(this).toggleClass(self.activeButtonClass);
        self.$checkAvailabilityTable.slideUp(200);
        self.$scheduleTourWrap.slideToggle(200, function(){
        });
      });
    },

    checkAvailability: function() {
      var self = this;

      // -- Toggle Table
      this.$checkAvailability.click(function(e){
        e.preventDefault();
        self.$scheduleTour.removeClass(self.activeButtonClass);
        $(this).toggleClass(self.activeButtonClass);
        self.$scheduleTourWrap.slideUp(200);
        self.$checkAvailabilityTable.slideToggle(200);
      });

      // -- Toggle Lease Term
      this.$leaseTermToggle.click(function(e){
        e.preventDefault();
        $('#lease-term-' + $(this).data('toggle-id')).toggle();
      });
    },

    ready: function() {
      this.dom();
      this.definitions();
      this.datepicker();
      this.dimensionToggle();
      this.floorToggle();
      this.scheduleTour();
      this.checkAvailability();
    }
  }

  $(function(){
    floorplan.ready();
  });
})();