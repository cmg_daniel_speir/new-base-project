(function(){

  var map = {
    domCache: function() {
      this.$mapCanvas = $('#map_canvas');
      this.$zoomIn    = $('#zoom_in');
      this.$zoomOut   = $('#zoom_out');
      this.$refresh   = $('#refresh');
      this.$poiFilter = $('[data-js-hook="poi-filter"]');
    },

    definitions: function() {
      // -- Probably don't want to touch.
      this.instance;
      this.mainInfoBox;
      this.poiMarkers    = [];
      this.basePath      = config.siteUrl;
      this.assetPath     = this.basePath + this.$mapCanvas.data('assets-path').replace(/^\/|\/$/g, '') + '/';
      this.lat           = this.$mapCanvas.data('lat');
      this.lng           = this.$mapCanvas.data('lng');
      this.address       = this.$mapCanvas.data('address');
      this.addressHeader = this.$mapCanvas.data('address-header');
      this.mapUrl        = this.$mapCanvas.data('map-url');
      this.pinpointSVG   = this.$mapCanvas.data('pinpoint-svg');
      this.pinpointLogo  = this.$mapCanvas.data('pinpoint-logo');

      // -- You can touch.
      this.zoomIncrement   = 1;
      this.defaultCategory = this.$mapCanvas.data('default-category');
      this.homeImageFile   = 'map_home.png';
      this.dataEndpoint    = 'neighborhood';
      this.poiIconFormat   = 'btn_{{category}}_small';
      this.mainInfoboxHTML = '<div class="map__infobox-modal">\
                                <div class="map__infobox-pinpoint-wrap ">\
                                  <div class="map__infobox-pinpoint-svg">' + this.pinpointSVG + '</div>\
                                  <img class="map__infobox-pinpoint-icon" src="' + this.pinpointLogo + '">\
                                </div>\
                                <div class="map__infobox-address">\
                                  <a class="map__infobox-close-button" id="close-main-infobox">&times;</a>\
                                  <p class="map__infobox-address-text">\
                                    <b class="map__infobox-address-header">' + this.addressHeader + '</b><br/>\
                                    ' + this.address + '\
                                  </p>\
                                  <small class="map__infobox-directions-text">\
                                    <a class="map__infobox-directions-link" href="' + this.mapUrl + '" target="_blank">Get Directions</a>\
                                  </small>\
                                </div>\
                               </div>';
      this.poiInfoboxHTML   = "<div class='map__infobox-poi'>\
                                <p class='map__infobox-poi-address'>\
                                  <b class='map__infobox-poi-address-header'>\
                                    {{name}}\
                                  </b>\
                                  {{address}}\
                                </p>\
                                <a class='map__infobox-poi-link' href='{{map_url}}' target='_blank'>\
                                  Get Directions\
                                </a>\
                              </div>";
      this.mapStyles = [
                        {
                          "featureType": "landscape.man_made",
                          "elementType": "labels",
                          "stylers": [{ "visibility": "off" }]
                        },
                        {
                          "featureType": "landscape",
                          "elementType": "geometry.fill",
                          "stylers": [{ "color": "#ffffff" }]
                        },
                        {
                          "featureType": "landscape.natural.terrain",
                          "elementType": "geometry.fill",
                          "stylers": [{ "color": "#000000" }]
                        },
                        {
                          "featureType": "landscape.natural.man_made",
                          "elementType": "geometry.fill",
                          "stylers": [{ "color": "#f2f0f0" }]
                        },
                        {
                          "featureType": "landscape.natural.landcover",
                          "elementType": "geometry.fill",
                          "stylers": [{ "visibility": "off" }]
                        },
                        {
                          "featureType": "poi",
                          "elementType": "geometry.fill",
                          "stylers": [{ "visibility": "off" }]
                        },
                        {
                          "featureType": "poi",
                          "elementType": "labels.text.fill",
                          "stylers": [{ "visibility": "off" }]
                        },
                        {
                          "featureType": "poi",
                          "elementType": "labels.icons",
                          "stylers": [{ "visibility": "off" }]
                        },
                        {
                          "featureType": "administrative",
                          "elementType": "labels.text.fill",
                          "stylers": [{ "color": "#a5a19e" }]
                        },
                        {
                          "featureType": "road.arterial",
                          "elementType": "geometry.fill",
                          "stylers": [{ "color": "#dfdcdb" }]
                        },
                        {
                          "featureType": "road.arterial",
                          "elementType": "geometry.stroke",
                          "stylers": [{ "color": "#cccccc" }]
                        },
                        {
                          "featureType": "road",
                          "elementType": "labels.text.fill",
                          "stylers": [{ "color": "#afaba8" }]
                        },
                        {
                          "featureType": "road",
                          "elementType": "labels.text.stroke",
                          "stylers": [{ "visibility": "off" }]
                        },
                        {
                          "featureType": "road.highway",
                          "elementType": "geometry.fill",
                          "stylers": [{ "color": "#dfdcdb" }]
                        },
                        {
                          "featureType": "road.highway",
                          "elementType": "geometry.stroke",
                          "stylers": [{ "color": "#c2bebc" }]
                        },
                        {
                          "featureType": "road.highway",
                          "elementType": "labels.text.fill",
                          "stylers": [{ "color": "#787471" }]
                        },
                        {
                          "featureType": "road.local",
                          "elementType": "geometry.fill",
                          "stylers": [{ "color": "#dfdcdb" }]
                        },
                        {
                          "featureType": "water",
                          "elementType": "geometry.fill",
                          "stylers": [{ "visibility": "off" }]
                        },
                      ]
    },

    /*
      Interval Object: closeButton

      Object's onLoad property fires its callback function
      when the $('#close-main-infobox') in the DOM has been
      loaded.
    */
    closeButton: {
      interval: 50,
      limit: 200,
      loop: 0,
      onLoad: function(callback) {
        var self = this;
        var interval;
        interval = setInterval(function(){
          ++self.loop;
          var $button = $('#close-main-infobox');
          var buttonLoaded = $button.length;
          if (buttonLoaded || self.loop === self.limit) {
            clearInterval(interval);
            if (buttonLoaded) {
              callback($button);
            }
          }
        }, this.interval);
      }
    },

    onGoogleLoad: function(callback){
      var self = this;
      google.load("search", "1.0", {
        callback: callback
      });
    },

    loadMap: function() {
      map.setOptions();
      map.googleMap = google.maps;
      map.instance = new map.googleMap.Map(map.$mapCanvas[0], map.mapOptions);
      map.bindControls();
      map.googleMap.event.addDomListener(window, 'resize', map.loadMap);

      // -- Place Home Marker
      var mainMarker = new map.googleMap.Marker({
        map: map.instance,
        position: map.latlng,
        icon : map.assetPath + map.homeImageFile
      });

      // -- Build InfoBox & Bind Toggle Controls
      map.mainInfoBox = new InfoBox(map.pinpointOptions);
      map.mainInfoBox.isMain = true;
      map.openMarker(map.mainInfoBox, mainMarker);
      map.bindMarkerOpen(map.mainInfoBox, mainMarker);

      // -- When Close Button loads, bind its click event
      map.closeButton.onLoad(function($button){
        $button.click(function(e){
          e.preventDefault();
          map.mainInfoBox.close();
        });
      });

      // -- Draw all POIs to map
      map.getPOIs(function(points){
        points.forEach(function(point){
          map.drawPoint(point);
        });

        map.poiVisibility(map.defaultCategory);
      });
    },

    drawPoint: function(point) {
      var latlng = new this.googleMap.LatLng(point.lat, point.lng);
      var poiInfoboxHTML = this.poiInfoboxHTML;
      var poiOptions = this.poiOptions;
      var poiIcon = this.poiIconFormat.replace(/{{category}}/g, point.category);

      // -- Mustache Var Replace
      for (property in point) {
        var expression = new RegExp('({{' + property + '}})', 'g');
        if (poiInfoboxHTML.match(expression)) {
          poiInfoboxHTML = poiInfoboxHTML.replace(expression, point[property]);
        }
      }

      // -- Build InfoBox Object
      poiOptions.content = poiInfoboxHTML;
      poiInfobox = new InfoBox(poiOptions);

      // -- Determine icon file extension. (.png if IE, .svg if not)
      if (navigator.userAgent.indexOf('rv:11') > -1 || navigator.userAgent.indexOf('MSIE') > -1){
        poiIcon += '.png';
      } else {
        poiIcon += '.svg';
      }

      // -- Draw point onto map.
      var poiMarker = new this.googleMap.Marker({
        map: map.instance,
        position: latlng,
        icon : map.assetPath + poiIcon
      });

      // -- Set 'category' property and push marker into array.
      poiMarker.category = point.category;
      this.poiMarkers.push(poiMarker);
      this.bindMarkerOpen(poiInfobox, poiMarker);
    },

    poiVisibility: function(category) {
      // -- Close all InfoBoxes except Main
      if (this.openWindow && !this.openWindow.isMain) {
        this.openWindow.close();
      }

      // -- Hide everything not matching passed-in category
      this.poiMarkers.forEach(function(marker){
        marker.setVisible(true);
        if (marker.category !== category) {
          marker.setVisible(false);
        }
      });
    },

    getPOIs: function(callback) {
      $.getJSON(this.basePath + this.dataEndpoint.replace(/^\/|\/$/g, ''), function(points){
        callback(points);
      });
    },

    bindMarkerOpen: function(infoBoxObject, markerObject) {
      var self = this;
      this.googleMap.event.addListener(markerObject, 'click', function(){
        self.openMarker(infoBoxObject, markerObject);
      });
    },

    openMarker: function(infoBoxObject, markerObject, callback) {
      // -- Close all InfoBoxes except Main
      if (this.openWindow && !this.openWindow.isMain) {
        this.openWindow.close();
      }
      // -- Open new InfoBox
      infoBoxObject.open(this.instance, markerObject);
      this.openWindow = infoBoxObject;
    },

    bindControls: function() {
      var self = this;

      // -- Adjust POI Visibility when selecting a filter
      this.$poiFilter.click(function(e){
        e.preventDefault();
        self.poiVisibility($(this).data('poi'));
      });

      // -- Map click closes all windows
      this.googleMap.event.addListener(self.instance, 'click', function(){
        self.mainInfoBox.close();
        if (self.openWindow) {
          self.openWindow.close();
        }
      });

      // -- Zoom In Control
      this.$zoomIn.click(function(e){
        e.preventDefault();
        self.instance.setZoom(self.instance.getZoom() + self.zoomIncrement);
      });

      // -- Zoom Out Control
      this.$zoomOut.click(function(e){
        e.preventDefault();
        self.instance.setZoom(self.instance.getZoom() - self.zoomIncrement);
      });

      // -- Refresh Control
      this.$refresh.click(function(e){
        e.preventDefault();
        self.loadMap();
      });
    },

    setOptions: function(){
      this.latlng = new google.maps.LatLng(this.lat, this.lng);

      // -- Point of Interest Infobox Options Object
      this.poiOptions = {
        content: null, // -- Set by drawPoint
        disableAutoPan: false,
        alignBottom: true,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(0, -30),
        zIndex: null,
        boxClass: "map__infobox",
        infoBoxClearance: new google.maps.Size(1, 1),
        isHidden: false,
        pane: "floatPane",
        enableEventPropagation: true
      };

      // -- Pinpoint Infobox Options Object
      this.pinpointOptions = {
        content: this.mainInfoboxHTML,
        disableAutoPan: false,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(-71, -160),
        zIndex: 9999,
        boxStyle: {},
        boxClass: "map__infobox",
        closeBoxMargin: "-20px 43px 0 0",
        closeBoxURL: "",
        infoBoxClearance: new google.maps.Size(1, 1),
        isHidden: false,
        pane: "floatPane",
        enableEventPropagation: true
      };

      // -- Map Options Object
      this.mapOptions = {
        zoom: 14,
        minZoom: 5,
        maxZoom: 25,
        scrollwheel:false,
        center: this.latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        disableDefaultUI: true,
        styles: this.mapStyles
      };
    },

    init: function() {
      this.domCache();
      this.definitions();
      this.onGoogleLoad(this.loadMap);
    }
  }

  $(document).ready(map.init());
})();