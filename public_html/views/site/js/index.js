(function(){

	var index = {
		dom: function() {
			this.$rotator = $('#rotator');
		},

		rotator: function() {
			this.$rotator.flexslider({
				controlNav: false,
				directionNav: true
			});
		},

		ready: function() {
			this.dom();
			this.rotator();
		}
	};

	$(function(){
		index.ready();
	});	
})();