(function(){
	var location = {
		dom: function() {
			this.$loadAll 	= $('#load-all-poi');
			this.$loadLocal = $('[data-js-hook="load-local-poi"]');
			this.$poi     	= $('[data-js-hook="poi-hidden"]');
		},

		loadAll: function() {
			var self = this;
			var moreText = this.$loadAll.text();
			var lessText = this.$loadAll.data('js-less');
			this.$loadAll.click(function(e){
				e.preventDefault();
				$(this).slideUp(200);
				self.$poi.slideToggle(200);
			});
		},

		loadLocal: function() {
			this.$loadLocal.click(function(e){
				e.preventDefault();
				$(this).toggleClass('location__poi-category-header--active');
				$('[data-js-category="' + $(this).data('js-open-category') + '"]').slideToggle(200);
			});
		},

		ready: function() {
			this.dom();
			this.loadAll();
			this.loadLocal();
		}
	}

	$(function(){
		location.ready();
	});
})();