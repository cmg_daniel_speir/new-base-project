(function(){
	
	var init = {
		dom: function() {
			this.$infieldLabel = $('[data-infield-label]');
			this.$moveInDate = $('#MoveInDate');
		},

		pickadate: function() {
      this.$moveInDate.pickadate({
        klass: {
          input: '',
          active: ''
        }
      });
    },

		infieldLabel: function() {
			this.$infieldLabel.inFieldLabels();
		},

		ready: function() {
			this.dom();
			this.infieldLabel();
			this.pickadate();
		}
	};

	$(function(){
		init.ready();
	});
})();