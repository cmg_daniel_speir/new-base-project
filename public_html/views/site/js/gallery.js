(function(){
  var gallery = {
    dom: function() {
      this.$swipebox = $('[data-js-hook="swipebox"]');
    },

    swipebox: function() {
      var self = this;

      this.$swipebox.swipebox({
        hideBarsDelay: false,
        removeBarsOnMobile: false,
        loopAtEnd: true,
        afterOpen: function(){
          var $swipeboxContainer = $('#swipebox-container');
          var $swipeboxClose = $('#swipebox-close');
          var imageBuffer = {
            interval: 200,
            limit: 100,
            loop: 0,
            onCompleted: function(callback){
              var interval;
              var self = this;
              interval = setInterval(function(){
                ++self.loop;
                if ($('#swipebox-slider .slide.current img').length || self.loop >= self.limit) {
                  clearInterval(interval);
                  callback();
                }
              }, this.interval);
            }
          };

          $swipeboxContainer.click(function(e){
            e.preventDefault();
            $swipeboxClose.trigger('click');
          });


          imageBuffer.onCompleted(function(){
            $('#swipebox-top-bar, #swipebox-slider .slide.current img').click(function(e){
              e.stopPropagation();
            });
          });
        }
      })
    },

    ready: function() {
      this.dom();
      this.swipebox();
    }
  };

  $(function(){
    gallery.ready();
  });
})();