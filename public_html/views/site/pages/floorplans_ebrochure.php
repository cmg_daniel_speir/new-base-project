<?php
	// -- eBrochure Settings
	require_once('includes/ebrochure/settings.php');

	$sPageModule = 'ebrochure_page';

	Page::modules($sPageModule, 'floorplan', 'ebrochure');

	$aFloorplans = array();
	$floorplan->reset();
	if ($floorplan->hasItems()) {
		foreach ($floorplan->getItems() as $oFloorplan) {
			if ($oFloorplan->get('pdf_download') && file_exists(str_replace($server_name, $doc_root, $oFloorplan->get('pdf_download')))) {
				$aFloorplans[] = $oFloorplan;
			}
		}
	}

	// -- Page Item
	$$sPageModule->reset();
	if ($$sPageModule->hasItems()) {
		$oPageItem = $$sPageModule->getItem();
	} else {
		die('Page could not be loaded.');
	}

	// -- Ebrochure
	$ebrochure->reset();
	$mEbrochure = $ebrochure->getItem();

	// -- SEO
	$page_name = 'EBROCHURE';
	require_once('includes/seo-setup.php');
	Page::title($page_title);
	Page::description($page_description);
	Page::keywords($page_keywords);
	Page::robots($page_robots);

	// -- Page Banner
	$aPageBannerSettings = array(
		'back_button' => array(
			'title' => '« Back to Floorplans',
			'url' => '/floorplans/'
		)
	);

	// -- Header
	require_once('includes/header.php');

	Page::addScript('ebrochure.js');
?>

<div class="page">
	<?php require_once('includes/partials/page-banner.php'); ?>
	<div class="page__container">
		<?php if ($mEbrochure && count($aFloorplans)): ?>
			<div class="page__content page__content--space-bottom page__content--align-center">
				<p class="page__title">Create your custom E-Brochure!</p>
		
				<p>Select the floor plan(s) you're interested in. Click Create e-Brochure. That's it!</p>
			</div>
			<div class="ebrochure">
				<form action="/floorplans/ebrochure/generate/" method="post" id="ebrochure">
					<table class="ebrochure__table">
						<tr class="ebrochure__row ebrochure__row--heading">
							<th class="ebrochure__heading ebrochure__heading--left">Floorplan</th>
							<th class="ebrochure__heading">Bed<span>room</span>s</th>
							<th class="ebrochure__heading">Bath<span>room</span>s</th>
							<th class="ebrochure__heading">Sq.Ft.</th>
						</tr>
						<?php foreach ($aFloorplans as $oFloorplan): ?>
							<?php if (file_exists(str_replace($server_name, $doc_root, $oFloorplan->get('pdf_download')))): ?>
								<tr class="ebrochure__row" data-js-hook="ebrochure-row" data-checkbox="fp<?php $oFloorplan->output('id'); ?>">
									<td class="ebrochure__cell ebrochure__cell--title">
										<input data-js-hook="checkbox" type="checkbox" id="fp<?php $oFloorplan->output('id'); ?>" name="fp<?php $oFloorplan->output('id'); ?>" value="<?php $oFloorplan->output('id'); ?>">
										<?php $oFloorplan->output('title'); ?>
									</td>
									<td class="ebrochure__cell">
										<?php $oFloorplan->output('bedrooms'); ?>
										<?php if (is_numeric($oFloorplan->get('bedrooms'))): ?>
											<span class="ebrochure__cell--unit"> Bed<?php if ($oFloorplan->get('bedrooms')>1): ?>s<?php endif; ?></span>
										<?php endif; ?>
									</td>
									<td class="ebrochure__cell"><?php $oFloorplan->output('bathrooms'); ?><span class="ebrochure__cell--unit"> Bathroom<?php if ($oFloorplan->get('bathrooms')>1): ?>s<?php endif; ?></span></td>
									<td class="ebrochure__cell"><?php $oFloorplan->output('square_feet'); ?><span class="ebrochure__cell--unit"> Sq. Ft.</span></td>
								</tr>
							<?php endif; ?>
						<?php endforeach; ?>
					</table>
					<p class="ebrochure__error" id="ebrochure-error"  style="display:none;">
						Please select at least 1 floorplan to create the e-Brochure!
					</p>
					<input class="ebrochure__button ebrochure__button--submit" type="submit" name="submit" id="" value="Create e-Brochure">
				</form>
			</div>
		<?php else: ?>
			<div class="page__content page__content--align-center">
				<h3>Whoops!</h3>
				<p>No E-Brochure and/or individual floorplan PDFs could be found with which to properly generate an E-Brochure.</p>
				<p><a href="/floorplans/">« Back to Floorplans</a></p>
			</div>
		<?php endif; ?>
	</div>
</div>

<?php require_once('includes/footer.php'); ?>
