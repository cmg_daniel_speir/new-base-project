<?php
// Define Page Module
$sPageModule = 'site_map_page';

// -- Module init
Page::modules($sPageModule);

// -- Page Item
$$sPageModule->reset();
$oPageItem = $$sPageModule->getItem();

// -- SEO
$page_name = 'SITE_MAP';
require_once('includes/seo-setup.php');
Page::title($page_title);
Page::description($page_description);
Page::keywords($page_keywords);
Page::robots($page_robots);

// -- Header
require_once('includes/header.php');

$bIs404 = true;
?>

<div class="page">
	<?php require_once('includes/partials/page-banner.php'); ?>
	<div class="page__container">
		<?php if ($sContent = $oPageItem->get('content')): ?>
			<div class="page__content page__content--space-bottom">
				<?php echo $sContent; ?>
			</div>
		<?php endif; ?>
		<?php require_once('includes/partials/site-map.php'); ?>
	</div>
</div>

<?php require_once('includes/footer.php'); ?>