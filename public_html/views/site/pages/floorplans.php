	<?php
	require_once('includes/functions/get_high_low.php');

	// Define Page Module
	$sPageModule = 'floorplans_page';

	Page::modules($sPageModule, 'floorplan', 'site_plan', 'available_apartment', 'floorplan_data');

	// -- Page Item
	$$sPageModule->reset();
	$oPageItem = $$sPageModule->getItem();

	// -- Site Plan
	$site_plan->reset();
	$mSitePlan = $site_plan->getItem();

  // -- Ensure oSiteConfig object
  if (!is_object($oSiteConfig)){
    Page::modules('site_configuration');
    $site_configuration->reset();
    $oSiteConfig = $site_configuration->getItem();
  }

	// -- Throw to Single
	if (getParam(0)){
		require_once('_floorplan-single.php');
		exit;
	}

	// -- Floorplan
	$floorplan->reset();
	$aFloorplans = array();

	if ($floorplan->hasItems()) {
		foreach ($floorplan->getItems() as $oFloorplan) {
			$available_apartment->reset();
		  $floorplan_data->reset();
		  $floorplan_data->filter('floorplan_group_id', $oFloorplan->get('floorplan_group_id'));

		  if ($oFloorplanData = $floorplan_data->getItem()) {
		  	$available_apartment->filter('floorplan', $oFloorplanData->get('id_value'));
			} 

			$aFloorplans[] = (object)array(
				'item' => $oFloorplan,
				'range' => get_rent_high_low($available_apartment, $floorplan_data, $oSiteConfig)
			);
		}
	}

	// -- SEO
	$page_name = 'FLOORPLANS';
	require_once('includes/seo-setup.php');
	Page::title($page_title);
	Page::description($page_description);
	Page::keywords($page_keywords);
	Page::robots($page_robots);


	$aPageBannerSettings = array(
		'left_button' => array(
			'title' => 'Site Map',
			'url' => 'http://google.com',
			'target' => '_blank'
		),
		'right_button' => array(
			'title' => 'E-Brochure',
			'url' => '/floorplans/ebrochure/',
			'target' => '_self'
		)
	);
	
	// -- Header
	require_once('includes/header.php');
?>

<div class="page">
	<?php require_once('includes/partials/page-banner.php'); ?>
	<?php if (count($aFloorplans)): ?>
		<div class="floorplan-listing" id="floorplan">
			<ul class="floorplan-listing__list">
				<?php foreach ($aFloorplans as $oFloorplan): ?>
					<li class="floorplan-listing__item">
						<a class="floorplan-listing__container" href="<?php $oFloorplan->item->permalink('floorplans'); ?>">
							<div class="floorplan-listing__table">
								<div class="floorplan-listing__column floorplan-listing__column--left">
									<?php if ($oFloorplan->item->get('sold_out') === 'Yes' && $oSiteConfig->get('sold_out_text')): ?>
										<p class="floorplan-listing__sold-out">
											<?php $oSiteConfig->output('sold_out_text'); ?>
										</p>
									<?php endif; ?>
									<img class="floorplan-listing__image" src="<?php $oFloorplan->item->output('listing_image'); ?>" alt="" />
								</div>
								<div class="floorplan-listing__column floorplan-listing__column--right">
									<div class="floorplan-listing__content">
										<p class="floorplan-listing__title color__secondary--text"><?php $oFloorplan->item->output('title'); ?></p>
										<p class="floorplan-listing__info"><?php echo $oFloorplan->item->output('bedrooms') . ' Bed | ' . $oFloorplan->item->get('bathrooms')  . ' Bath | ' . $oFloorplan->item->get('square_feet') . ' sq. ft.'; ?></p>
										<?php if ($oSiteConfig->get('show_pricing_ranges') && $oFloorplan->range['low'] && $oFloorplan->range['high']): ?>
											<p class="floorplan-listing__info floorplan-listing__info--price">
												<?php  
													echo $oFloorplan->range['low'];
													echo $oFloorplan->range['low'] !== $oFloorplan->range['high'] ? ' - ' . $oFloorplan->range['high'] : null;
												?>
											</p>
										<?php endif; ?>
									</div>
									<div class="floorplan-listing__details-button">
				            Details
          				</div>
								</div>
							</div>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
			<?php if ($oPageItem->get('disclaimer')): ?>
				<div class="page__disclaimer">
					<p class="page__disclaimer-text"><?php $oPageItem->output('disclaimer'); ?></p>
				</div>
			<?php endif; ?>
		</div>
	<?php elseif($sFallbackMessage = $oPageItem->get('floorplan_fallback_message')): ?>
		<div class="page__container">
			<div class="page__content page__content--align-center">
				<?php echo companyNameReplace($sFallbackMessage); ?>
			</div>
		</div>
	<?php endif; ?>
</div>
<?php require_once('includes/footer.php'); ?>