<?php  
// -- Page Name
$sPageName = 'amenities_page';

// -- Modules
Page::modules($sPageName, 'amenity_group', 'amenity');

// -- Page Item
$$sPageName->reset();
if (!$oPageItem = $$sPageName->getItem()) {
	die('Page not found.');
}

// -- Amenity Group
$amenity_group->reset();
$amenity_group->limit(2);

// -- SEO
$page_name = 'AMENITIES';
require_once('includes/seo-setup.php');
Page::title($page_title);
Page::description($page_description);
Page::keywords($page_keywords);
Page::robots($page_robots);

// -- Header
require_once('includes/header.php');

// -- View Scripts
Page::addScript('amenities.js');
?>

<div class="page">
	<?php require_once('includes/partials/page-banner.php'); ?>
	<div class="page__container">
		<div class="amenities">
			<div class="amenities__group-wrap">
				<?php foreach ($amenity_group->getItems() as $oGroup): ?>
					<?php
						$amenity->reset();
						$amenity->filter('amenity_group', $oGroup->get('name'));
						$iCount = 0;
						$iAmenityLimit = 6;
						$bLocalLoadMore = false;
					?>
					<div class="amenities__group">
						<?php if ($oGroup->get('image')): ?>
							<img class="amenities__image" src="<?php $oGroup->output('image', 'width=500&height=500&crop=1'); ?>" alt="" />
						<?php endif; ?>
						<p class="amenities__group-title"><?php $oGroup->output('name'); ?></p>
						<ul class="amenities__list">
							<?php foreach ($amenity->getItems() as $oAmenity): ?>
								<?php
									if (count($amenity->getItems()) > $iAmenityLimit){
										$bGlobalLoadMore = true;
										$bLocalLoadMore = true;
									}
									$iCount++;
								?>
								<li class="amenities__item<?php if ($iCount >= $iAmenityLimit): ?> amenities__item--hidden<?php endif; ?>"
										<?php if ($iCount >= $iAmenityLimit): ?>
											data-js-hook="amenity-hidden"
											data-amenity-group="<?php $oGroup->output('id') ?>"
										<?php endif; ?>
								>
									<p class="amenities__text">
										<?php
											$oAmenity->output('feature');
											echo $oAmenity->get('select_units') === 'Yes' ? '*' : '';
										?>
									</p>
								</li>
							<?php endforeach; ?>
						</ul>
						<?php if ($bLocalLoadMore): ?>
						 	<a data-js-hook="load-local-amenity" data-trigger-amenity-group="<?php $oGroup->output('id') ?>" class="amenities__load-more amenities__load-more--local" target="" href="#">
								Load All
							</a>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>

			<?php if ($bGlobalLoadMore): ?>
			 	<a id="load-all-amenity" class="amenities__load-more amenities__load-more--global" target="" href="">
					Load All Amenities
				</a>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php require_once('includes/footer.php'); ?>