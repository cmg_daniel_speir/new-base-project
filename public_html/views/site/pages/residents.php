<?php 
	$sPageModule = 'residents_page';

	Page::modules($sPageModule);

	$$sPageModule->reset();
	if (!$oPageItem = $$sPageModule->getItem()) {
		die('Page not found.');
	}	

	// -- Determine Button Existence
	$bHasButtonOne = ($oPageItem->get('button_link') && $oPageItem->get('button_display_text'));
	$bHasButtonTwo = ($oPageItem->get('button_link_2') && $oPageItem->get('button_display_text_2'));

	// -- SEO
	$page_name = 'RESIDENTS';
	require_once('includes/seo-setup.php');
	Page::title($page_title);
	Page::description($page_description);
	Page::keywords($page_keywords);
	Page::robots($page_robots);

	require_once('includes/header.php'); 
?>

<div class="page">
	<?php require_once('includes/partials/page-banner.php'); ?>

	<div class="page__container">
		<div class="page__content page__content--align-center">
			<p><?php $oPageItem->output('content'); ?></p>
		</div>

		<?php if ($bHasButtonOne || $bHasButtonTwo): ?>
			<div class="page__button-wrap">
				<?php if ($bHasButtonOne): ?>
					<a target="<?php echo $oPageItem->get('button_target'); ?>" class="page__button" href="<?php echo formatUrl($oPageItem->get('button_link')); ?>">
						<?php $oPageItem->output('button_display_text'); ?>
					</a>
				<?php endif; ?>
				<?php if ($bHasButtonTwo): ?>
					<a target="<?php echo $oPageItem->get('button_target_2'); ?>" class="page__button" href="<?php echo formatUrl($oPageItem->get('button_link_2')); ?>">
						<?php $oPageItem->output('button_display_text_2'); ?>
					</a>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
</div>

<?php require_once('includes/footer.php'); ?>