<?php
	// Define Page Module
	$sPageModule = 'location_page';

	// -- Module init
	Page::modules($sPageModule, 'poi');

  if (isAjax()) {
    $poi->reset();
    $oPoiItemntsOfInterest = array();

    // foreach ($poi->getItems() as $oPoiItem) {
    //   $oPOI = new stdClass();
    //   $oPOI->category = $aPoi->get('category_name');
    //   $oPOI->name = $aPoi->get('name');
    //   $oPOI->address = $aPoi->get('street_address') . '<br />' . $aPoi->get('city') . ', ' . $aPoi->get('state') . ' ' . $aPoi->get('zip');
    //   $oPOI->map_url = 'http://maps.google.com/maps?q=' . str_replace('&', '', $oPOI->name) . ' ' . str_replace('<br />', ' ', $oPOI->address);
    //   $oPOI->lat = $aPoi->get('lat');
    //   $oPOI->lng = $aPoi->get('lng');
    //   $aPointsOfInterest[] = $oPOI;
    // }

    echo json_encode(array_map(function($oPoiItem){
      $oPOI = new stdClass();
      $oPOI->category = $oPoiItem->get('category_name');
      $oPOI->name = $oPoiItem->get('name');
      $oPOI->address = $oPoiItem->get('street_address') . '<br />' . $oPoiItem->get('city') . ', ' . $oPoiItem->get('state') . ' ' . $oPoiItem->get('zip');
      $oPOI->map_url = 'http://maps.google.com/maps?q=' . str_replace('&', '', $oPOI->name) . ' ' . str_replace('<br />', ' ', $oPOI->address);
      $oPOI->lat = $oPoiItem->get('lat');
      $oPOI->lng = $oPoiItem->get('lng');
      return $oPOI;
    }, $poi->getItems()));

    exit;
  }

	// -- Page Item
	$$sPageModule->reset();
	$oPageItem = $$sPageModule->getItem();

  // -- Ensure oSiteIcons object
  if (!is_object($oSiteIcons)){
    Page::modules('seo_images');
    $seo_images->reset();
    $oSiteIcons = $seo_images->getItem();
  }

  // -- Ensure oSiteConfig object
  if (!is_object($oSiteConfig)){
    Page::modules('site_configuration');
    $site_configuration->reset();
    $oSiteConfig = $site_configuration->getItem();
  }

  // -- Ensure Contact Page Object
  if (!is_object($oContact)){
    Page::modules('contact_page');
    $contact_page->reset();
    $oContact = $contact_page->getItem();
  }

	// -- Build Address
  if ($oContact->get('address_2')){
    $sAddress = $oContact->get('address') . '<br />' . $oContact->get('address_2') . '<br />' . $oContact->get('city') . ', ' . $oContact->get('state') . ' ' . $oContact->get('zip');
  } else {
    $sAddress = $oContact->get('address') . '<br />' . $oContact->get('city') . ', ' . $oContact->get('state') . ' ' . $oContact->get('zip');
  }

  // -- Determine Map Logo
  if (!$sLogo = $oSiteIcons->get('map_logo')) {
    $sLogo = $oSiteConfig->get('logo');
  }

  // -- Retrieve Categories
  $oPoiBuilder = new ModuleBuilder('poi');
  $aCategories = $oPoiBuilder->getSelectOptions('category_name');
  $bLoadMore = false; // -- Assume none more to load

  // -- SEO
  $page_name = 'LOCATION';
  require_once('includes/seo-setup.php');
  Page::title($page_title);
  Page::description($page_description);
  Page::keywords($page_keywords);
  Page::robots($page_robots);

	// -- Header
	require_once('includes/header.php');

	// -- Page Scripts
  Page::addScript('vendors/infobox.js');
  Page::addScript('map.js');
  Page::addScript('location.js');
?>

<div class="page">
  <?php require_once('includes/partials/page-banner.php'); ?>
  <div class="page__body">
    <div class="map">
      <div class="map__canvas"
           id="map_canvas"
           data-default-category="<?php echo $aCategories[0]['key']; ?>"
           data-assets-path="views/site/images/map"
           data-map-url="<?php $oContact->output('map_url'); ?>"
           data-address="<?php echo $sAddress; ?>"
           data-address-header="<?php $oContact->output('location_name'); ?>"
           data-lat="<?php $oContact->output('lat'); ?>"
           data-lng="<?php $oContact->output('lng'); ?>"
           data-pinpoint-svg="<?php echo file_get_contents('views/site/images/map/pinpoint-icon.svg'); ?>"
           data-pinpoint-logo="<?php echo $sLogo; ?>"
      >
      </div>
      <div class="map__nav">
        <div class="map__nav-item map__nav-item--control map__nav-item--zoom-out" id="zoom_out"></div>
        <div class="map__nav-item map__nav-item--control map__nav-item--zoom-in" id="zoom_in"></div>
        <div class="map__nav-item map__nav-item--control map__nav-item--refresh" id="refresh"></div>
        <div class="map__nav-poi-wrap">
          <div class="map__nav-item map__nav-item--poi map__nav-item--restaurants" data-js-hook="poi-filter" data-poi="category1"></div>
          <div class="map__nav-item map__nav-item--poi map__nav-item--shopping" data-js-hook="poi-filter" data-poi="category3"></div>
          <div class="map__nav-item map__nav-item--poi map__nav-item--recreation" data-js-hook="poi-filter" data-poi="category4"></div>
          <div class="map__nav-item map__nav-item--poi map__nav-item--coffee"data-js-hook="poi-filter" data-poi="category2"></div>
        </div>
      </div>
    </div>

    <div class="location">
      <div class="location__mobile-address">
        <div class="location__mobile-address-container">
          <p class="location__mobile-address-content">
            <span class="location__mobile-address-header"><?php echo $oContact->get('location_name'); ?></span>
            <?php
              echo $oContact->get('address') . '<br />';
              if ($oContact->get('address_2')) {
                echo $oContact->get('address_2') . '<br />';
              }
              echo $oContact->get('city') . ', ' . $oContact->get('state') . ' ' . $oContact->get('zip');
            ?>
          
          </p>
          <a class="location__mobile-address-link" target="_blank" href="<?php echo $oContact->output('map_url'); ?>">
            Get Directions
          </a>
        </div>
      </div>

      <div class="location__poi">
        <div class="location__poi-container">
          <div class="location__poi-category-wrap">
            <?php foreach ($aCategories as $aCategory): ?>
              <?php
                $iPoiCount = 0;
                $iPoiLimit = 3;
                $poi->reset();
                $poi->filter('category_name', $aCategory['key']);
                $poi->orderBy('name ASC');
                if ($poi->hasItems()):
              ?>
                <div class="location__poi-category">
                  <p class="location__poi-category-header location__poi-category-header--desktop"><?php echo $aCategory['value']; ?></p>
                  <a href="#" data-js-hook="load-local-poi" data-js-open-category="<?php echo $aCategory['key']; ?>" class="location__poi-category-header location__poi-category-header--mobile"><?php echo $aCategory['value']; ?></a>
                  <div class="location__poi-category-list-wrap">
                    <ul class="location__poi-category-list" data-js-category="<?php echo $aCategory['key']; ?>">
                      <?php foreach ($poi->getItems() as $oPoi): ?>
                        <?php
                          $iPoiCount++;
                          if ($iPoiCount > $iPoiLimit) {
                            $bHidden = true;
                            $bLoadMore = true;
                          } else {
                            $bHidden = false;
                          }
                        ?>
                        <li <?php if ($bHidden): ?>data-js-hook="poi-hidden" <?php endif; ?>class="location__poi-category-list-item <?php if ($bHidden): ?>location__poi-category-list-item--hidden<?php endif; ?>">
                          <p class="location__poi-category-title">
                            <?php $oPoi->output('name'); ?>
                          </p>
                        </li>
                      <?php endforeach; ?>
                    </ul>
                  </div>
                </div>
              <?php endif; ?>
            <?php endforeach; ?>
          </div>
          <?php if ($bLoadMore): ?>          
            <a id="load-all-poi" class="location__poi-load-more" href="#">
              Load All
            </a>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once('includes/footer.php'); ?>