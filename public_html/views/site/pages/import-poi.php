<?php  

	/* -----------------------------------------------------------------
  	You can edit this.
	----------------------------------------------------------------- */
	// -- Location Modules & Fields
	$sLocationModule = 'contact_page';
	$sLatField = 'lat';
	$sLngField = 'lng';

	// -- POI Modules & Fields
	$sPoiModule = 'poi';
	$sCategoryField = 'category_name';

	/* -----------------------------------------------------------------
  	Probably don't.
	----------------------------------------------------------------- */	
	if (isAjax() && isset($_POST['pois'])) {
		$oDb = DbAbstractionContent::getInstance();
		$aPOIs = json_decode($_POST['pois']);
		$aPOIValues = array();
		$aColumnStructure = array();
		$aSerializeColumns = array('category_name', 'state');

		// -- Obtain column structure via the first POI object supplied to the Post.
		foreach ($aPOIs[0] as $sColumn => $sValue) {
			$aColumnStructure[] = $sColumn;
		}

		// -- Use the ascertained column structure to build the local aPoiValue array from
		// -- the POI object, and to add it to the larger aPOIValues array.
		foreach ($aPOIs as $oPOI) {
			$aPoiValue = array();
			foreach ($aColumnStructure as $sColumn) {
				// -- Serialize & escape necessary columns. 
				if (in_array($sColumn, $aSerializeColumns)) {
					$sPoiValue = $oDb->myAddSlashes(serialize($oPOI->{$sColumn}));
				} else {
					$sPoiValue = $oPOI->{$sColumn};
				}
				$aPoiValue[] = '"' . $sPoiValue . '"';
			}
			$aPOIValues[] = '(' . implode(', ', $aPoiValue) . ')';
		}

		// -- Drop all records with place_ids and import new records.
		try {
			$oDb->execute("DELETE FROM `$sPoiModule` WHERE `place_id` is not null");
			$oDb->execute("INSERT INTO `$sPoiModule` (" . implode(',', $aColumnStructure) . ") VALUES " . implode(',', $aPOIValues));
			echo "OK";
		} catch (Exception $e) {
			echo "ERROR";
		}
		exit;
	}

	Page::modules($sLocationModule, $sPoiModule);

	// -- Location Module
	$oLocationModule = $$sLocationModule;
	$oLocationModule->reset();

	if (!$mLocation = $oLocationModule->getItem()) {
		die("No record found in `$sLocationModule`.");
	}

	$iLat = $mLocation->get($sLatField);
	$iLng = $mLocation->get($sLngField);

	// -- Poi Module
	$oPoiBuilder = new ModuleBuilder($sPoiModule);
  $aCategories = $oPoiBuilder->getSelectOptions($sCategoryField);
  $aCategoryKeyIndex = array();
  foreach ($aCategories as $aCategory) {
  	$aCategoryKeyIndex[$aCategory['key']] = $aCategory['value'];
  }
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="NOINDEX, NOFOLLOW" />
	<title><?php echo Page::setting('company_name'); ?> | POI Importer</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>

	<div class="container" id="v-mount-poi-importer">
		<input type="hidden" v-model="categories | json" value='<?php echo json_encode($aCategoryKeyIndex); ?>'>
		<div class="page-header">
			<h1>
				POI Importer
				<a href="#" v-on:click="reset" class="btn btn-danger pull-right">Reset</a>
			</h1>
		</div>	

		<div class="panel panel-primary">
			<div class="panel-heading">Settings for <?php echo Page::setting('company_name'); ?></div>
			<div class="panel-body">	
				<div class="form" style="max-width:300px;">
				  <div class="form-group">
				    <label class="">Lattitude</label>
				    <input type="text" class="form-control" v-model="lat" value="<?php echo $iLat; ?>" placeholder="Lat" number>
				  </div>
				  <div class="form-group">
				    <label class="">Longitude</label>
				    <input type="text" class="form-control" v-model="lng" value="<?php echo $iLng; ?>" placeholder="Lng" number>
				  </div>
				  <div class="form-group">
				    <label>Search Radius</label>
				    <div class="input-group">
				      <input type="text" class="form-control" v-model="radius" placeholder="Radius">
				      <div class="input-group-addon">Meters</div>
				    </div>
				  </div>
				  <div class="form-group">
				    <label>Default Selection Limit</label>
				    <input type="text" class="form-control" v-model="defaultLimit" placeholder="Default Selection Limit">
				  </div>
				</div>
			</div>
		</div>

		<div class="panel panel-primary">
			<div class="panel-heading">Category Mapper</div>
			<div class="panel-body">	
				<div class="form-inline">
				  
				  <ul class="nav nav-pills nav-justified">
					  <li role="presentation" v-for="option in categoryOptions" v-bind:class="{ 'active': option.value == selectedCategory }">
					  	<a js-hook="tab" href="#" v-on:click="switchCategory(option.value, $event)">
					  		{{option.text}}
					  		<span v-if="categoryTypeMap[option.value].length" class="badge">{{categoryTypeMap[option.value].length}}</span>
					  		<span v-else class="badge">0</span>
					  	</a>
					  </li>
					</ul>

				  <div class="panel panel-default" style="margin-top: 30px;">
					  <div class="panel-heading">Mapping {{selectedCategoryDisplay}}</div>
				  	<div class="panel-body">
				  		<i>Select Google Place Types to map to current POI categories in the CMS.</i>
				  		<br /><br />
							<div class="form-group">
						    <label>Google Place Types:&nbsp;&nbsp;</label>
						    <select class="form-control" v-model="selectedType">
								  <option v-for="type in types" value="{{type}}">{{type}}</option>
								</select>
						  </div>
						  <br /><br />
							<div v-if="categoryTypeMap[selectedCategory].length">
								<ul class="list-group">
									<li v-for="category in categoryTypeMap[selectedCategory]" class="list-group-item">
										{{category}}
										<a v-on:click="removeType(category, $event)" href="#" class="label label-danger pull-right">&times;</a>
									</li>
								</ul>
						  </div>
						  <div v-else class="alert alert-warning">		  
						  	Nothing Mapped to {{selectedCategoryDisplay}}.
						  </div>
					  </div>
				  </div>
				</div>
			</div>
		</div>
		<button class="btn btn-primary" v-on:click="search">Run Search</button>
		<br /><br />
		<div class="panel panel-primary" v-show="visibilityToggleSearch">
			<div class="panel-heading">POI Results</div>
			<div class="panel-body">
				<i>First {{defaultLimit}} POIs are visible and pre-selected. Click "Show All" to see all results for a particular category.</i>
				<br /><br />
				<div v-for="(category, results) in poiResults" v-if="results.length">
					<div v-if="poiResultsStatus[category].complete" class="panel panel-default">
						<div class="panel-heading">
							{{categories[category]}}
							<div class="badge">{{results.length}}</div>
						</div>
						<ul class="list-group">
							<li v-for="(index, result) in results" class="list-group-item" v-show="result._jonah.show">
								<div class="checkbox" style="margin: 0;">
							    <label>
							      <input type="checkbox" value="{{result.place_id}}" v-model="poiSelections[category]"> 
							      {{result.name}}
							    </label>
							  </div>
							</li>
							<li class="list-group-item" v-if="results.length > 15">
								<a href="#" class="btn btn-primary" v-on:click="showAllPOIs(category, $event)">Show All {{categories[category]}}</a>
							</li>
						</ul>
					</div>
					<div v-else class="progress" style="margin-bottom: 0;">
					  <div class="progress-bar progress-bar-striped progress-bar-warning active" role="progressbar" aria-valuenow="100" aria-valuemin="100" aria-valuemax="100" style="width: 100%;">
					    <span>Searching for nearby {{categories[category]}}</span>
					  </div>
					</div>
				</div>
			</div>
		</div>
		<div v-show="importProgress.show" class="progress">
		  <div class="progress-bar progress-bar-striped active progress-bar-{{importProgress.class}}" role="progressbar" aria-valuenow="{{importProgress.current}}" aria-valuemin="0" aria-valuemax="100" style="width: {{importProgress.current}}%;">
		    <span>{{importProgress.message}}</span>
		  </div>
		</div>
		<a href="#" class="btn btn-primary" v-if="visibilityToggleImport && !visibilityToggleImportCancel" v-on:click="import">Run Import</a>
		<a href="#" class="btn btn-danger" v-if="visibilityToggleImportCancel" v-on:click="cancelImport">Cancel Import</a>
	</div>
	
	<footer>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYXL5l3jEhKRLRNi1Z-lp3BgRribHxuuY&libraries=places&callback=init"></script>
	  <script>
	  function init() {
	  	window.onload = function() {
	  		vPoiImporter.mapServiceInit();
	  	}
	  }
	  </script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.15/vue.js"></script>
		<script src="/views/site/js/import-poi.vue.js"></script>
	</footer>
</body>
</html>