<?php
	$sPageModule = 'ebrochure_page';

	Page::modules($sPageModule, 'floorplan', 'ebrochure');

	// Variables needed for str replacement
	require_once('includes/ebrochure/settings.php');
	require_once('includes/ebrochure/functions.php');

	$download_id = (int)$_REQUEST['download'];
	$download_file = $doc_root . 'pdfs/ebrochure' . $download_id . '.pdf';
	$download_link = $server_name . '/pdfs/ebrochure' . $download_id . '.pdf';
	if (!isset($download_id) || !file_exists($download_file))
	{
	  header("location: /floorplans/ebrochure/");
	  exit;
	}

	// -- Floorplans
	$floorplan->reset();

	// -- Ebrochure
	$ebrochure->reset();
	if ($ebrochure->hasItems()) {
		$oEbrochure = $ebrochure->getItem();
	} else {
		header("location: /floorplans/ebrochure/");
	}
	
	// -- Page Item
	$$sPageModule->reset();
	if ($$sPageModule->hasItems()) {
		$oPageItem = $$sPageModule->getItem();
	} else {
		die('Page could not be loaded.');
	}

	// -- SEO
	$page_name = 'EBROCHURE';
	require_once('includes/seo-setup.php');
	Page::title($page_title);
	Page::description($page_description);
	Page::keywords($page_keywords);
	Page::robots($page_robots);

	// -- Page Banner
	$aPageBannerSettings = array(
		'back_button' => array(
			'title' => '« Back to E-Brochure',
			'url' => '/floorplans/ebrochure/'
		)
	);

	// -- Header
	require_once('includes/header.php');

	Page::addScript('ebrochure.js');
?>

<div class="page">
	<?php require_once('includes/partials/page-banner.php'); ?>
	<div class="page__container">
		<div class="page__content page__content--space-bottom page__content--align-center">
			<h5>Your custom E-Brochure is ready!</h5>
			<p>Click below to download, email, or do both.</p>
			<div class="ebrochure">
				<a class="ebrochure__button ebrochure__button--center" target="_blank" href="<?php echo $download_link; ?>">View E-Brochure</a>
			</div>
		</div>
	
		<div class="page__content page__content--align-center">
			<h5>Email E-Brochure</h5>
		</div>

    <div class="ebrochure__form" id="ebrochure_form" <?php if (isset($_REQUEST['sent'])): ?>style="display:none;"<?php else: ?>style="display: block;"<?php endif; ?>>
      <form action="/floorplans/ebrochure/email/go/" id="ebrochure-email" method="post">
        <div class="ebrochure__input-wrap">
          <input class="ebrochure__input" type="email" name="brochureEmail" class="form__input--email required" auto-capitalize="off" placeholder="Email Address" />
        </div>
        <input type="hidden" name="download_id" value="<?php echo $download_id; ?>" />
        <input class="ebrochure__button ebrochure__button--submit ebrochure__button--center" type="submit" name="submit" value="Email E-Brochure">
      </form>
    </div>

		<div class="form">
			<div class="form__confirm" id="contact-confirm" <?php if (!isset($_REQUEST['sent'])): ?> style="display:none;"<?php endif; ?>>
			  <p class="form__confirm-header">Email sent!</p>
			  <p class="form__confirm-text">Want to send it to someone else? <a style="color: #fff; text-decoration: underline;" href="/floorplans/ebrochure/download/?download=<?php echo $download_id;?>">Share e-Brochure.</a></p>
			</div>
		</div>
  </div>
</div>

<?php require_once('includes/footer.php'); ?>
