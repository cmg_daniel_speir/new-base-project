<ul class="site-map">
  <?php $iNavCount = 1; ?>
  <?php foreach ($aNavigation as $sDisplay => $sLink): ?>
    <li class="site-map__item <?php if ($iNavCount === count($aNavigation) && ($iNavCount % 2 !== 0)): ?>site-map__item--last<?php endif; ?>">
      <a class="site-map__link" href="<?php echo $sLink; ?>">
        <?php echo $sDisplay; ?>
      </a>
    </li>
    <?php $iNavCount++; ?>
  <?php endforeach; ?>
</ul>
