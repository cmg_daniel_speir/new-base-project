<?php
  // -- Manual order by square foot
  $aAvailableApartments = array();
  foreach ($available_apartment->getItems() as $oAvailableApartment){
    $aAvailableApartments[$oAvailableApartment->get('id')] = (int)round($oAvailableApartment->get('rent_min'));
  }
  asort($aAvailableApartments);
?>
<div class="check-availability" id="check-availability-table" style="display:none;">
  <table class="check-availability__table">
    <tr class="check-availability__row check-availability__row--head">
      <th class="check-availability__head">Apt #</th>
      <th class="check-availability__head">Floor</th>
      <th class="check-availability__head">Rent</th>
      <th class="check-availability__head">Deposit</th>
      <th class="check-availability__head">Availability</th>
      <th class="check-availability__head">Action</th>
    </tr>
    <?php foreach ($aAvailableApartments as $iId => $iRent): ?>
      <?php
        $available_apartment->reset();
        $available_apartment->filter('id', $iId);
        $oAvailableApartment = $available_apartment->getItem();
        $lease_term->reset();
        $lease_term->filter('apartment', $oAvailableApartment->get('id_value'));

        // If the deposit or rent comes back as a decimal and is equal ".0000", don't show it (RealPage)
        $sAvailAptDeposit = number_format(str_replace('$', '', $oAvailableApartment->get('deposit')), 2);
        $sAvailAptRent = number_format(str_replace('$', '', $oAvailableApartment->get('rent_min')), 2);

        //now the values have guaranteed 2 deceimals. If they're ".00", get rid if it
        $sAvailAptDeposit = str_replace('.00', '', $sAvailAptDeposit);
        $sAvailAptRent = str_replace('.00', '', $sAvailAptRent);

        //Add the dollar sign to the text
        $sAvailAptDeposit = '$' . $sAvailAptDeposit;
        $sAvailAptRent = '$' . $sAvailAptRent;

        // Set the lease url based on type of integration being used
        if ($oSiteConfig->get('type') == 'resman') {
          $sLeaseUrl = 'https://' . $oApiSettingsItem->get('subdomain') . '.myresman.com/Portal/Applicants/Apply?accountID=' . $oApiSettingsItem->get('account_id') . '&propertyID=' . $oApiSettingsItem->get('property_id') . '&unit=' . $oAvailableApartment->get('id_value') . '&leaseTerm={lease_term}';
        } elseif ($oSiteConfig->get('type') == 'realpage') {
          //these links appear in different fomats on varsityquarters.com, venueatdinkytown.com, and liveatwesternstation.com
          $sLeaseUrl = $oSiteConfig->get('lease_link');
          $sLeaseUrl = str_replace(array('{site_id}', '{pmc_id}', '{unit_id}'), array($oApiSettingsItem->get('site_id'), $oApiSettingsItem->get('pmc_id'), $oAvailableApartment->get('id_value')), $sLeaseUrl);
        } elseif ($oSiteConfig->get('type') == 'psi') {
          $sLeaseUrl = 'https://' . $oApiSettingsItem->get('api_subdomain');
          $sLeaseUrl .= '.prospectportal.com/Apartments/module/application_authentication/popup/false/kill_session/1/property%5Bid%5D/' .$oApiSettingsItem->get('property_id');
          $sLeaseUrl .= '/property_floorplan%5Bid%5D]/' . $oAvailableApartment->get('floorplan') . '/';
        }
      ?>
      <tr class="check-availability__row">
        <td class="check-availability__cell">
          <span>Apt #: </span><?php $oAvailableApartment->output('apartment_number'); ?>
        </td>
        <?php if ($oAvailableApartment->get('floor')): ?>
          <td class="check-availability__cell">
            <span>Floor: </span><?php $oAvailableApartment->output('floor'); ?>
          </td>
        <?php else: ?>
          <td class="check-availability__cell">
            <span>Floor: </span>---
          </td>
        <?php endif; ?>
        <td class="check-availability__cell">
          <span>Rent: </span><?php echo $sAvailAptRent; ?>
        </td>
        <td class="check-availability__cell">
          <span>Deposit: </span><?php echo $sAvailAptDeposit; ?>
        </td>
        <td class="check-availability__cell">
          <span>Availability: </span><?php $oAvailableApartment->output('availability'); ?>
        </td>
        <?php if ($lease_term->hasItems()): ?>
          <td class="check-availability__cell">
            <a href="#" class="check-availability__cell-link" data-js-hook="toggle-lease-term" data-toggle-id="<?php echo $iId; ?>">View Lease Terms</a>
          </td>
        <?php else: ?>
          <td class="check-availability__cell">
            <a class="check-availability__cell-link"  href="<?php echo $sLeaseUrl; ?>" target="_blank">Lease Now</a>
          </td>
        <?php endif; ?>
      </tr>
      <tr class="check-availability__row check-availability__row--lease-term" id="lease-term-<?php echo $iId; ?>" style="display:none;">
        <?php if ($lease_term->hasItems()): ?>
          <td class="check-availability__cell check-availability__cell--lease-term" colspan="5">
            <b class="check-availability__lease-term-title">Select a Lease Term</b>
            <?php foreach ($lease_term->getItems() as $oLeaseTerm): ?>
              <?php
                // If the lease term rent comes back as a decimal and is equal ".00", don't show it
                $iLeaseTermRent = '$' . str_replace('.00', '', $oLeaseTerm->get('rent'));
              ?>
              <label class="check-availability__lease-term-option">
                <input class="check-availability__lease-term-input" type="radio" name="lease_term_<?php echo $count; ?>" value="<?php $oLeaseTerm->output('term'); ?>" checked="checked">
                <?php $oLeaseTerm->output('term'); ?> months at <?php echo $iLeaseTermRent; ?>/mo
              </label>
            <?php endforeach; ?>
          </td>
          <td class="check-availability__cell check-availability__cell--link">
            <a class="check-availability__cell-link check-availability__cell-link--grey" href="<?php echo $sLeaseUrl ?>" data-id="<?php echo $count ?>" target="_blank">Lease Now</a>
          </td>
        <?php else: ?>
          <td class="check-availability__cell" colspan="6">There are currently no lease terms set for this unit</td>
        <?php endif; ?>
      </tr>
    <?php endforeach; ?>
  </table>
</div>
