<?php if (isset($oPageItem)): ?>
	<div class="page__banner">
		<div title="<?php $oPageItem->output('title'); ?>" class="page__banner-image" style="background-image: url(<?php $oPageItem->output('banner'); ?>);"></div>
		<div class="page__banner-header">
			<div class="page__banner-header-container">
				<h2 class="page__banner-title"><?php $oPageItem->output('title'); ?></h2>

				<?php if (isset($aPageBannerSettings) && count($aPageBannerSettings)): ?>

					<?php if (isset($aPageBannerSettings['back_button']) && isset($aPageBannerSettings['back_button']['title']) && isset($aPageBannerSettings['back_button']['url'])): ?>
						<a class="page__banner-back-button" href="<?php echo $aPageBannerSettings['back_button']['url']; ?>">
							<?php echo $aPageBannerSettings['back_button']['title']; ?>
						</a>
					<?php endif; ?>

					<div class="page__banner-button-container">
						<?php if (isset($aPageBannerSettings['left_button']) && isset($aPageBannerSettings['left_button']['title']) && isset($aPageBannerSettings['left_button']['url'])): ?>
							<a class="page__banner-button page__banner-button--left" href="<?php echo $aPageBannerSettings['left_button']['url']; ?>" target="<?php echo isset($aPageBannerSettings['left_button']['target']) ? $aPageBannerSettings['left_button']['target'] : '_blank'; ?>">
								<?php echo $aPageBannerSettings['left_button']['title']; ?>
							</a>
						<?php endif; ?>

						<?php if (isset($aPageBannerSettings['right_button']) && isset($aPageBannerSettings['right_button']['title']) && isset($aPageBannerSettings['right_button']['url'])): ?>
							<a class="page__banner-button page__banner-button--right" href="<?php echo $aPageBannerSettings['right_button']['url']; ?>" target="<?php echo isset($aPageBannerSettings['right_button']['target']) ? $aPageBannerSettings['right_button']['target'] : '_blank'; ?>">
								<?php echo $aPageBannerSettings['right_button']['title']; ?>
							</a>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>