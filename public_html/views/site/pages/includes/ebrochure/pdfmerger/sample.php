<?php
include 'PDFMerger.php';

$pdf = new PDFMerger;

$pdf->addPDF('file1.pdf', 'all')
	->addPDF('file2.pdf', 'all')
	->merge('file', 'final.pdf');

	//REPLACE 'file' WITH 'browser', 'download', 'string', or 'file' for output options
	//You do not need to give a file path for browser, string, or download - just the name.
