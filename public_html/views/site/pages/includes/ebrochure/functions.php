<?php

function send_ebrochure_email($to_email,$download_link,$from_email='',$font_type='serif',$text_color='#000000',$header_image='')
{
  global $doc_root;

    if (!is_valid_ebrochure_email($to_email))
    {
      return false;
    }

    if (empty($from_email) ||!is_valid_ebrochure_email($from_email))
    {
      $from_email = Page::setting('email_from_account');
    }

    $company_name = Page::setting('company_name');
    $company_website = Page::setting('company_website');

    $body_html = getEBrochureEmailFileContents($doc_root . 'views/site/pages/includes/ebrochure/ebrochure_email.html');
    $body_text = getEBrochureEmailFileContents($doc_root . 'views/site/pages/includes/ebrochure/ebrochure_email.txt');

    $body_html = str_replace('{{company_name}}',$company_name,$body_html);
    $body_html = str_replace('{{year}}',date('Y'),$body_html);
    $body_html = str_replace('{{text_color}}',$text_color,$body_html);
    $body_html = str_replace('{{header_image}}',$header_image,$body_html);
    $body_html = str_replace('{{font_type}}',$font_type,$body_html);
    $body_html = str_replace('{{company_website}}',$company_website,$body_html);
    $body_html = str_replace('{{download_link}}',$download_link,$body_html);
    $body_text = str_replace('{{company_name}}',$company_name,$body_text);
    $body_text = str_replace('{{year}}',date('Y'),$body_text);
    $body_text = str_replace('{{company_website}}',$company_website,$body_text);
    $body_text = str_replace('{{download_link}}',$download_link,$body_text);

    $subject = "$company_name E-Brochure";


    // SETUP THE PHPMAILER
    $mail = new PHPMailer();

    $mail->From = $from_email;
    $mail->FromName = $send_from_name;
    $mail->AddReplyTo($from_email, $from_email);
    $mail->WordWrap = 50;
    $mail->IsHTML(true);

    $mail->Subject = $subject;
    $mail->Body    = $body_html;
    $mail->AltBody = $body_text;

    $mail->AddAddress($to_email);
    // $mail->AddBCC('yuri@jonahsystems.com');

    if(!$mail->Send())
    {
        echo "An error occurred while processing<br>";
    } else {
        echo "Success!";
    }

    $mail->ClearAddresses();
    $mail->ClearBCCs();
    $mail->SmtpClose();

}

function is_valid_ebrochure_email($email)
{
    if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email))
    {
        return false;
    }

    $email_array = explode("@", $email);

    $local_array = explode(".", $email_array[0]);

    for ($i = 0; $i < sizeof($local_array); $i++)

    {
        if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i]))
        {
            return false;
        }

    }

    if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1]))
    {
        $domain_array = explode(".", $email_array[1]);
        if (sizeof($domain_array) < 2)
        {
            return false;
        }

        for ($i = 0; $i < sizeof($domain_array); $i++)
        {
            if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i]))
            {
                return false;
            }
        }
    }
    return true;
}

function getEBrochureEmailFileContents($file)
{
    if ( file_exists($file) && ($fpointer = @fopen($file, 'rb')) )
    {
        $size = filesize($file);
        $contents = fread($fpointer, $size);
        fclose($fpointer);
        return $contents;
    }
    else
    {
      die("can't find file");
    }
}

?>