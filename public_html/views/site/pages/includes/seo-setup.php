<?php
Page::modules('seo');

// if the seo pagename is not set, then use generic
if (empty($page_name)) {
	$page_name = 'GENERIC';
}

// filter by the pagename
$seo->reset();
$seo->filter('name', $page_name);

if ($seo->hasItems()){
	$seo_item = $seo->getItem();
	if ($seo_item->get('page_title')){$page_title = $seo_item->get('page_title');}
	if ($seo_item->get('meta_description')){$page_description = $seo_item->get('meta_description');}
	if ($seo_item->get('meta_keywords')){$page_keywords = $seo_item->get('meta_keywords');}
	if ($seo_item->get('index')){$page_robots = $seo_item->get('index');}
	if ($seo_item->get('follow')){if (empty($page_robots)){$page_robots = $seo_item->get('follow');}else{$page_robots .= ',' . $seo_item->get('follow');}}

	$company_name = Page::setting("company_name");
	$page_title = str_replace('{COMPANY_NAME}',$company_name,$page_title);
	$page_description = str_replace('{COMPANY_NAME}',$company_name,$page_description);
	$page_keywords = str_replace('{COMPANY_NAME}',$company_name,$page_keywords);

}
?>