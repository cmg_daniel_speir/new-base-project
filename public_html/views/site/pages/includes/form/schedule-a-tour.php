<?php
  Page::modules('floorplan', 'lead_source', 'floorplan_data');

  // -- Lead Source
  $lead_source->reset();
  $lead_source->orderBy('display_name');

  // -- Floorplans
  $floorplan->reset();
?>
<form class="form" action="/schedule-a-tour/go/" method="post" id="schedule-a-tour">
  <div class="form__row">
    <div class="form__column">
      <label for="FirstName" class="form__label form__label--infield" data-infield-label>
        First Name
        <span class="form__required">*</span>
      </label>
      <input type="text" name="FirstName" id="FirstName" class="form__field form__field--text form__field--white-bg" />
    </div>
    <div class="form__column">
      <label for="LastName" class="form__label form__label--infield" data-infield-label>
        Last Name
        <span class="form__required">*</span>
      </label>
      <input type="text" name="LastName" id="LastName" class="form__field form__field--text form__field--white-bg" />
    </div>
  </div>
  <div class="form__row">
    <div class="form__column">
      <label for="Phone" class="form__label form__label--infield" data-infield-label>
        Phone
        <span class="form__required">*</span>
      </label>
      <input type="tel" name="Phone" id="Phone" class="form__field form__field--text form__field--white-bg" />
    </div>
    <div class="form__column">
      <label for="Email" class="form__label form__label--infield" data-infield-label>
        Email
        <span class="form__required">*</span>
      </label>
      <input type="text" name="Email" id="Email" autocapitalize="off" class="form__field form__field--text form__field--white-bg" />
    </div>
  </div>
  <div class="form__column"  style="display:none;"><?php /* Honeypot */  ?>
    <label for="Email_Again" class="form__label form__label--infield">Email Address</label>
    <input type="email" name="Email_Again" id="Email_Again" />
  </div>
  <div class="form__row">
    <div class="form__column form__column--select">
      <select name="Floorplan" id="Floorplan" class="form__field form__field--select form__field--white-bg">
        <option value="">Select a Floorplan</option>
        <?php if ($floorplan->hasItems()): ?>
          <?php foreach ($floorplan->getItems() as $floor_plan_item): ?>
            <?php
              // Bedroom count comes in as a decimal value from resman. This is to only output the number before the decimal.
              $aBedroomCountParts = explode('.', $floor_plan_item->get('bedrooms'));

              $sFloorplanIdValue = '';
              $floorplan_data->reset();
              $floorplan_data->filter('floorplan_group_id', $floor_plan_item->get('floorplan_group_id'));
              if ($floorplan_data->hasItems()) {
                $oData = $floorplan_data->getItem();
                $sFloorplanIdValue = $oData->get('id_value');
              }
            ?>
            <option value="<?php echo $floor_plan_item->get('title') . '|' . $sFloorplanIdValue; ?>" <?php if (isset($iSelectedIndex) && $iSelectedIndex !== -1 && $aFloorplans[$iSelectedIndex]->get('id') == $floor_plan_item->get('id')): ?>selected<?php endif; ?>><?php echo $floor_plan_item->get('title') . ' (' . $aBedroomCountParts[0] . ' Bed/' . $floor_plan_item->get('bathrooms') . ' Bath)'; ?></option>
          <?php endforeach; ?>
        <?php endif; ?>
      </select>
    </div>
    <div class="form__column">
      <label for="MoveInDate" class="form__label form__label--infield" data-infield-label>Move-in Date</label>
      <input class="form__field form__field--text form__field--white-bg" type="text" name="MoveInDate" id="MoveInDate" gldp-id="datePicker" readonly />
    </div>
  </div>
  <?php $lead_source->reset(); ?>
  <?php $lead_source->orderBy('display_name'); ?>
  <?php if ($lead_source->hasItems()): ?>
  <div class="form__row form__row--select">
    <select name="LeadSource" id="LeadSource" class="form__field form__field--select form__field--white-bg form__field--placeholder">
      <option value="">How Did You Hear About Us?</option>
      <?php foreach ($lead_source->getItems() as $oLeadSourceItem): ?>
      <option value="<?php $oLeadSourceItem->output('value') ?>"><?php $oLeadSourceItem->output('display_name') ?></option>
      <?php endforeach;?>
    </select>
  </div>
  <?php endif; ?>
  <div class="form__row">
    <div class="input_wrap">
      <label for="CommentsQuestions" class="form__label form__label--infield" data-infield-label>Questions</label>
      <textarea class="form__field form__field--textarea form__field--white-bg" name="CommentsQuestions" id="CommentsQuestions"></textarea>
    </div>
  </div>
  <div class="form__row">
    <div class="form__loading" id="form-loading" style="display:none;">
      <img class="form__loading-icon" src="/views/site/images/icons/loading.gif" alt="Form Submitting" title="Form Submitting" />
    </div>
    <input type="submit" class="form__field form__field--submit" name="lease_submit" id="lease_submit" value="Submit Form" />
  </div>
</form>
<div class="form__confirm" id="schedule-tour-confirm" style="display: none;">
  <p class="form__confirm-header">Thank You!</p>
  <p class="form__confirm-message">Your submission was successful. We will get back with you shortly.</p>
</div>