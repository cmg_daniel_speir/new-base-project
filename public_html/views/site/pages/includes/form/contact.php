<div class="form">
	<form id="contact-form" method="POST" action='/contact-go/'>
		<div class="form__row">
			<label for="name" class="form__label form__label--infield" data-infield-label>Full Name <span class="form__label--required">*</span></label>
			<input id="name" name="Name" type="text" class="form__field form__field--text" />
		</div>
		<div class="form__row">
			<label for="email" class="form__label form__label--infield" data-infield-label>Email <span class="form__label--required">*</span></label>
			<input id="email" name="Email" type="text" class="form__field form__field--text" />
		</div>
		<div class="form__row">
			<label for="message" class="form__label form__label--infield" data-infield-label>Message <span class="form__label--required">*</span></label>
			<textarea id="message" name="Message" class="form__field form__field--textarea"></textarea>
		</div>
		<div class="form__row">
			<input name="email_again" type="hidden" class="form__field form__field--text" />
			<input type="submit" value="Send It!" class="form__field form__field--submit" />
		</div>
	</form>
	<div class="form__confirm" id="contact-confirm" style="display: none;">
	  <p class="form__confirm-header">Thank You!</p>
	  <p class="form__confirm-message">Your submission was successful. We will get back with you shortly.</p>
	</div>
</div>