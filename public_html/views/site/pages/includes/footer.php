
<div style="display: table; margin: 0 auto; position:relative;">
	<a id="share-link" rel="nofollow" href="#">Share This</a>
	<div class="share-loader" id="share-loader" style="display:none;">
		<div class="share-loader__circle share-loader__circle--1"></div>
		<div class="share-loader__circle share-loader__circle--2"></div>
		<div class="share-loader__circle share-loader__circle--3"></div>
	</div>
  <div class="share-this-popup" id="share-this-popup" data-script="<?php echo urlTo('share-this-popup'); ?>" data-tweet-filter="GLOBAL">
    <!-- populated via AJAX -->
  </div>
</div>

<?php if (isPage('neighborhood')): ?>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?v=3.1&sensor=false"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<?php endif; ?>
<?php Page::scripts(); ?>
</body>
</html>