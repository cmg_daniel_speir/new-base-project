<?php

if (!function_exists('analytics')) {
  function analytics($sService) {
    $analytics = new ModuleContent('analytics');
    $analytics->reset();
    $mAnalytics = $analytics->getItem();
    if ($mAnalytics && $mAnalytics->get('analytics') == 'On' && $mAnalytics->get('service') == $sService) {
      $sCode = $mAnalytics->get('code');

      switch ($sService) {
        case 'google_analytics':
          echo "
          <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', '$sCode', 'auto');
            ga('send', 'pageview');
          </script>" . PHP_EOL;
          break;
        case 'google_tag_manager':
          echo "
          <noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=$sCode\"
          height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
          <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','$sCode');</script>" . PHP_EOL;

          if ($mAnalytics->get('contact_us_remarketing_active') == 'On' || $mAnalytics->get('schedule_tour_remarketing_active') == 'On') {
            $aRemarketing = array(
              'contact_us_remarketing_active' => $mAnalytics->get('contact_us_remarketing_active'),
              'contact_us_remarketing_code' => $mAnalytics->get('contact_us_remarketing_code'),
              'contact_us_remarketing_html' => $mAnalytics->get('contact_us_remarketing_html', 'escape'),
              'schedule_tour_remarketing_active' => $mAnalytics->get('schedule_tour_remarketing_active'),
              'schedule_tour_remarketing_code' => $mAnalytics->get('schedule_tour_remarketing_code'),
              'schedule_tour_remarketing_html' => $mAnalytics->get('schedule_tour_remarketing_html', 'escape'),
            );

            foreach ($aRemarketing as $sName => $sValue) {
              echo "<input type='hidden' id='$sName' value='$sValue' />" . PHP_EOL;
            }
          }
        break;
      }
    }
  }
}

/*
  companyNameReplace

  Easily detects and replaces {COMPANY_NAME} in contnet
*/
if (!function_exists('companyNameReplace')){
  function companyNameReplace($sContent) {
    return str_replace('{COMPANY_NAME}', Page::setting('company_name'), $sContent);
  }
}

/*
  getPhoneLink
  @param sPhone | String

  Echo "tel:" link with formatted phone number (non-digits stripped).
*/
if (!function_exists('getPhoneLink')){
  function getPhoneLink($sPhone) {
    return 'tel:' . preg_replace('/\D/', '', $sPhone);
  }
}

/*
  formatUrl
  @param sUrl | String

  Format user-input URL to output properly
*/
if (!function_exists('formatUrl')){
  function formatUrl($sUrl) {
    if (preg_match("(https:\/\/|http:\/\/)", $sUrl)) {
      return $sUrl;
    } else {
      return '/' . preg_replace("/\s/", '', rtrim(ltrim($sUrl, '/'), '/')) . '/';
    }
  }
}

/*
  isActive
  @param mActiveUri | Mixed

  Determine if string provided matches current URI, or
  if current URI is contained with the array provided.
*/
if (!function_exists('isActive')){
  function isActive($mActiveUri) {

    /*
      _getActiveState
      @param sActiveUri | String

      Determine if URI provided is the current URI, or "Active".
    */
    if (!function_exists('_getActiveState')) {
      function _getActiveState($sActiveUri) {
        if ($sActiveUri == 'index') {
          $sActiveUri = '/';
        }
        $sActiveUri = rtrim(ltrim($sActiveUri, '/'), '/');
        $aActiveUri = explode('/', $sActiveUri);
        if (count($aActiveUri) === 1 && Dispatcher::getSegment(1)) {
          return $aActiveUri[0] === Dispatcher::getSegment(0);
        } elseif (count($aActiveUri)) {
          $sRequestUri = str_replace('?' . $_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']);
          if (getParam(0)) {
            $aUri = explode(getParam(0), $sRequestUri);
            $sUri = $aUri[0];
          } else {
            $sUri = $sRequestUri;
          }
          return $sActiveUri === rtrim(ltrim($sUri, '/'), '/');
        } else {
          return false;
        }
      }
    }

    switch (gettype($mActiveUri)) {
      case 'string':
        return _getActiveState($mActiveUri);
        break;
      case 'array':
        $bReturn = false;
        foreach ($mActiveUri as $sActiveUri) {
          if (_getActiveState($sActiveUri)) {
            $bReturn = true;
            break;
          }
        }
        return $bReturn;
        break;
    }
  }
}

/*
  hex2rgb
  @param color | String

  Converts Hex to RGB value
*/
function hex2rgb( $colour ) {
  if ( $colour[0] == '#' ) {
          $colour = substr( $colour, 1 );
  }
  if ( strlen( $colour ) == 6 ) {
          list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
  } elseif ( strlen( $colour ) == 3 ) {
          list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
  } else {
          return false;
  }
  $r = hexdec( $r );
  $g = hexdec( $g );
  $b = hexdec( $b );
  return implode(',', array( 'red' => $r, 'green' => $g, 'blue' => $b ));
}

/*
  adjustBrightness
  @param hex | String
  @param steps | Integer

  Adjusts given hex value a number of steps in a specified direction
*/
function adjustBrightness($hex, $steps) {
  // Steps should be between -255 and 255. Negative = darker, positive = lighter
  $steps = max(-255, min(255, $steps));

  // Normalize into a six character long hex string
  $hex = str_replace('#', '', $hex);
  if (strlen($hex) == 3) {
      $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
  }

  // Split into three parts: R, G and B
  $color_parts = str_split($hex, 2);
  $return = '#';

  foreach ($color_parts as $color) {
      $color   = hexdec($color); // Convert to decimal
      $color   = max(0,min(255,$color + $steps)); // Adjust color
      $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
  }

  return $return;
}

/*
	includeSvg
	@param svgFile | String
*/
if (!function_exists('includeSVG')){
  function includeSVG($sSVGFile, $sFolder = 'svgs'){
    $sDirectory = $_SERVER['DOCUMENT_ROOT'] . '/views/site/images/' . $sFolder . '/';
    //If user hasn't entered ".svg", we'll do it for them.
    if (strpos($sSVGFile, '.svg') != false){
      include($sDirectory . trim($sSVGFile));
    } else {
      include($sDirectory . trim($sSVGFile) . '.svg');
    }
  }
}