<?php

/**
 * Gets the list of available apartments and then uses the Site Config's "Price Range Source" setting to determine how
 * to get the low/high rent value for each apartment.
 * 
 * @param object $available_apartment      ModuleContent item
 * @param object $floorplan_data           ModuleContent item
 * @param object $oSiteConfigItem          Site Configuration module record item
 * @return string
 */
function get_rent_high_low($available_apartment, $floorplan_data, $oSiteConfigItem) {
  $rents = array();
  $rents['low'] = '';
  $rents['high'] = '';
  
  $sSourceType = $oSiteConfigItem->get('price_range_source');
  switch ($sSourceType) {
    case 'apartment_then_floorplan':
      //Get the low and high from available apartments for this floorplan.
      //After doing that if we don't have a low and/or high defined, then fallback
      //and get the value from the floorplans.
      $rents = getFromApartments($available_apartment);
      if ($rents['low'] == '' || $rents['high'] == '') {
        $rentsFallback = getFromFloorplan($floorplan_data);
        if ($rents['low'] == '') {
          $rents['low'] = $rentsFallback['low'];
        }
        if ($rents['high'] == '') {
          $rents['high'] = $rentsFallback['high'];
        }
      }
      break;

    case 'combined':
      //Get the lowest value looking at both floorplans and available units.
      //Get the highest value looking at both floorplans and available units.
      $rentsFloorplans = getFromFloorplan($floorplan_data);
      $rentsApartments = getFromApartments($available_apartment);

      $rents['low'] = $rentsFloorplans['low'] < $rentsApartments['low'] ? $rentsFloorplans['low'] : $rentsApartments['low'];
      //if one of them empty then it equates to being < the other. So here we'll make sure we have a value set, if not, then pull from one of the 2 that IS set.
      $rents['low'] = empty($rents['low']) ? empty($rentsApartments['low']) ? $rentsFloorplans['low'] : $rentsApartments['low'] : $rents['low'];

      $rents['high'] = $rentsFloorplans['high'] > $rentsApartments['high'] ? $rentsFloorplans['high'] : $rentsApartments['high'];
      //if one of them empty then it equates to being < the other. So here we'll make sure we have a value set, if not, then pull from one of the 2 that IS set.
      $rents['high'] = empty($rents['high']) ? empty($rentsApartments['high']) ? $rentsFloorplans['high'] : $rentsApartments['high'] : $rents['high'];
      break;
    
    case 'floorplan':
    default:
      //Get the low/high from the floorplans
      $rents = getFromFloorplan($floorplan_data);
      break;
  }

  if (!empty($rents['low'])) {
    $rents['low'] = '$' . number_format($rents['low'], 0);
  }
  if (!empty($rents['high'])) {
    $rents['high'] = '$' . number_format($rents['high'], 0);
    ;
  }
  return $rents;
}

/**
 * Get the low and high price from the floorplan_data records.
 * 
 * @param ModuleContent $floorplan_data
 * @return array
 */
function getFromFloorplan($floorplan_data) {
  $rents = array('high'=>'', 'low'=>'');
  
  if ($floorplan_data->hasItems()) {
    foreach ($floorplan_data->getItems() as $oFloorplanItem) {
      $min = $oFloorplanItem->get('rent_min');
      $min = (int)round(str_replace("$", '', $min));
      if ($rents['low'] == '' || $min < $rents['low']) {
        $rents['low'] = $min;
      }
      $max = $oFloorplanItem->get('rent_max');
      $max = (int)round(str_replace('$', '', $max));
      if ($rents['high'] == '' || $max > $rents['high']) {
        $rents['high'] = $max;
      }
    }
  }
  return $rents;
}

/**
 * Get the low and high price from the available_apartment records.
 * 
 * @param ModuleContent $available_apartment
 * @return array
 */
function getFromApartments($available_apartment) {
  $rents = array('high'=>'', 'low'=>'');
  
  if ($available_apartment->hasItems()) {
    foreach ($available_apartment->getItems() as $oApartmentItem) {
      $min = $oApartmentItem->get('rent_min');
      $min = (int)round(str_replace('$', '', $min));
      if ($rents['low'] == '' || $min < $rents['low']) {
        $rents['low'] = $min;
      }
      $max = $oApartmentItem->get('rent_max');
      $max = (int)round(str_replace('$', '', $max));
      if ($rents['high'] == '' || $max > $rents['high']) {
        $rents['high'] = $max;
      }
    }
  }
  return $rents;
}