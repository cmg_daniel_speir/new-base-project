<?php  
ini_set('display_errors', 1);

Page::modules('seo_images', 'analytics', 'site_configuration', 'contact_page', 'social_network_account');

// -- Styles
Page::addStyle('screen.css');

// -- Scripts
Page::addScript('vendors/jquery.js');
Page::addScript('vendors/formulate.js');
Page::addScript('vendors/infield.js');
Page::addScript('vendors/pickadate.js');
Page::addScript('init.js');
Page::addScript('share-this-popup.js');

// -- Site Map
$aNavigation = array(
	'Floor Plans' => '/floorplans/',
	'Amenities' => '/amenities/',
	'Neighborhood' => '/neighborhood/',
	'Gallery' => '/gallery/',
	'Residents' => '/residents/',
	'Contact' => '/contact/'
);

// -- Site Config
$site_configuration->reset();
$oSiteConfig = $site_configuration->getItem();

// -- Contact Page
$contact_page->reset();
$mContact = $contact_page->getItem();

// -- Social Network Account
$social_network_account->reset();

// -- SEO Images
$seo_images->reset();
if ($seo_images->hasItems()){
  $seo_image_item = $seo_images->getItem();
  $ico = $seo_image_item->getRawValue('favicon');
  $ico_path = '/assets/images/' . $ico[0]->sLocation;
}

// -- Analytics
$analytics->reset();
$mAnalytics = $analytics->getItem();

// -- Custom Functions
require_once('functions/custom-functions.php');
?>
<!DOCTYPE html>
<html xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="<?php Page::keywords(); ?>" />
	<meta name="description" content="<?php Page::description(); ?>" />
	<meta name="robots" content="<?php Page::robots(); ?>" />
	<meta name="author" content="<?php echo Page::setting('company_name'); ?>" />
	<meta name="copyright" content="&copy; <?php echo date('Y'); ?> <?php echo Page::setting('company_name'); ?>" />
	<meta name="generator" content="Jonah Systems, LLC - www.jonahsystems.com" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta property="og:title" content="<?php Page::title(); ?>" />
	<meta property="og:description" content="<?php Page::description(); ?>" />
	<meta property="og:url" content="<?php echo currentUrl(true); ?>" />
	<meta property="og:image" content="<?php $seo_image_item->output('og_image'); ?>" />
	<meta property="og:image:type" content="image/jpeg" />

	<link rel="apple-touch-icon-precomposed" href="<?php $seo_image_item->output('ipad_icon'); ?>" />
	<link rel="shortcut icon" href="<?php echo $ico_path; ?>" />

	<title><?php Page::title(); ?></title>	

	<?php  
		Page::debugMin(true);
		Page::styles();
	?>

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300italic,300,400italic' rel='stylesheet' type='text/css'>

	<?php analytics('google_analytics'); ?>
</head>
<body>
	<?php analytics('google_tag_manager'); ?>

