<?php
	Page::modules('leasing_office');

	// -- Ensure oContact object
	if (!is_object($oContact)){
		Page::modules('contact_page');
		$contact_page->reset();
		if ($contact_page->hasItems()){
	 		$oPageItem = $contact_page->getItem();
		}
	} else {
		$oPageItem = $oContact;
	}

	// -- Ensure oLeasingOffice object
	if (!is_object($oLeasingOffice)){
		Page::modules('leasing_office');
		$leasing_office->reset();
		if ($leasing_office->hasItems()){
	 		$oLeasingOffice = $leasing_office->getItem();
		}
	}

	// -- Office Hours
	$iOfficeHourCount = 0;
	$aOfficeHours = array();
	while ($iOfficeHourCount < 4){
		$iOfficeHourCount++;
		if ($oPageItem->get("day_range_$iOfficeHourCount") && $oPageItem->get("time_range_$iOfficeHourCount")){
			$aOfficeHour = array(
				'day_range'  => $oPageItem->get("day_range_$iOfficeHourCount"),
				'time_range' => $oPageItem->get("time_range_$iOfficeHourCount")
			);
			$aOfficeHours[] = $aOfficeHour;
		}
	}

	// -- SEO
	$page_name = 'CONTACT';
	require_once('includes/seo-setup.php');
	Page::title($page_title);
	Page::description($page_description);
	Page::keywords($page_keywords);
	Page::robots($page_robots);

	// -- Header
	require_once('includes/header.php');

	// -- Page Scripts
	Page::addScript('vendors/formulate.js');
	Page::addScript('forms/contact-validate.js');
?>

<div class="page">
	<?php require_once('includes/partials/page-banner.php'); ?>
	<div class="page__container">
		<div class="contact">
			<div class="contact__row">
				<ul class="contact__column contact__column--info">
					<li class="contact__item contact__item--address">
						<a target="_blank" class="contact__link contact__link--address" href="<?php echo $oPageItem->output('map_url'); ?>">
							<p class="contact__address-header"><?php $oPageItem->output('location_name'); ?></p>
				      <?php
				        echo $oPageItem->get('address') . '<br />';
				        if ($oPageItem->get('address_2')) {
				          echo $oPageItem->get('address_2') . '<br />';
				        }
				        echo $oPageItem->get('city') . ', ' . $oPageItem->get('state') . ' ' . $oPageItem->get('zip');
				      ?>
      			</a>
					</li>
					<?php if ($oPageItem->get('email')): ?>
						<?php
							$aEmail = explode('@', $oPageItem->get('email'));
						?>
						<li class="contact__item">
							<a class="contact__link" data-mail-name="<?php echo $aEmail[0]; ?>" data-mail-host="<?php echo $aEmail[1] ?>">
								Email Us
							</a>
						</li>
					<?php endif; ?>
					<?php if ($oPageItem->get('phone')): ?>
						<li class="contact__item">
							<a class="contact__link color__primary--text" href="tel:<?php $oPageItem->output('phone'); ?>">
								Phone: <?php $oPageItem->output('phone'); ?>
							</a>
						</li>
					<?php endif; ?>
					<?php if ($oPageItem->get('fax')): ?>
						<li class="contact__item">
							<a href="tel:<?php $oPageItem->output('fax'); ?>" class="contact__link color__primary--text">
								Fax: <?php $oPageItem->output('fax'); ?>
							</a>
						</li>
					<?php endif; ?>
				</ul>

				<?php if ($oLeasingOffice): ?>
					<ul class="contact__column contact__column--info">
						<li class="contact__item contact__item--address">
							<a target="_blank" class="contact__link contact__link--address" href="<?php echo $oLeasingOffice->output('map_url'); ?>">
								<p class="contact__address-header">Leasing Office</p>
					      <?php
					        echo $oLeasingOffice->get('address') . '<br />';
					        if ($oLeasingOffice->get('address_2')) {
					          echo $oLeasingOffice->get('address_2') . '<br />';
					        }
					        echo $oLeasingOffice->get('city') . ', ' . $oLeasingOffice->get('state') . ' ' . $oLeasingOffice->get('zip');
					      ?>
	      			</a>
						</li>
						<?php if ($oLeasingOffice->get('email')): ?>
							<?php
								$aEmail = explode('@', $oLeasingOffice->get('email'));
							?>
							<li class="contact__item">
								<a class="contact__link" data-mail-name="<?php echo $aEmail[0]; ?>" data-mail-host="<?php echo $aEmail[1] ?>">
									Email Us
								</a>
							</li>
						<?php endif; ?>
						<?php if ($oLeasingOffice->get('phone')): ?>
							<li class="contact__item">
								<a class="contact__link color__primary--text" href="tel:<?php $oLeasingOffice->output('phone'); ?>">
									Phone: <?php $oLeasingOffice->output('phone'); ?>
								</a>
							</li>
						<?php endif; ?>
						<?php if ($oLeasingOffice->get('fax')): ?>
							<li class="contact__item">
								<a href="tel:<?php $oLeasingOffice->output('fax'); ?>" class="contact__link color__primary--text">
									Fax: <?php $oLeasingOffice->output('fax'); ?>
								</a>
							</li>
						<?php endif; ?>
					</ul>
				<?php else: ?>
					<a href="<?php echo $oPageItem->output('map_url'); ?>" target="_blank" class="contact__column contact__column--map">
						<div class="contact__map-content">
							<img class="contact__map-icon" src="/views/site/images/icons/map.png" alt="" />
							<p class="contact__map-text">Get Directions</p>
						</div>
					</a>
				<?php endif; ?>

			</div>

			<?php if (count($aOfficeHours)): ?>
				<div class="contact__office-hour <?php if ($oLeasingOffice): ?>contact__office-hour--alt<?php endif; ?>">
					<p class="contact__office-hour-column contact__office-hour-column--left contact__office-hour-header <?php if ($oLeasingOffice): ?>contact__office-hour-header--alt<?php endif; ?>">Office Hours</p>
					<ul class="contact__office-hour-column contact__office-hour-column--right contact__office-hour-list">
						<?php foreach ($aOfficeHours as $aOfficeHour): ?>
							<li class="contact__office-hour-item <?php if ($oLeasingOffice): ?>contact__office-hour-item--alt<?php endif; ?>">
								<p class="contact__office-hour-content contact__office-hour-content--day <?php if ($oLeasingOffice): ?>contact__office-hour-content--alt<?php endif; ?>">
									<?php echo $aOfficeHour['day_range']; ?>
								</p>
								<p class="contact__office-hour-content contact__office-hour-content--time <?php if ($oLeasingOffice): ?>contact__office-hour-content--alt<?php endif; ?>">
									<?php echo $aOfficeHour['time_range']; ?>
								</p>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>

			<div class="contact__form-wrap">
				<h2 class="contact__form-header">Drop us a line!</h2>
				<div class="contact__form-wrap-container">
					<?php require_once('includes/form/contact.php'); ?>
		    </div>
		  </div>
		</div>
	</div>
</div>

<?php require_once('includes/footer.php'); ?>