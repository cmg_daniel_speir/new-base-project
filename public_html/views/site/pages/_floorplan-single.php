<?php
	require_once('includes/functions/get_high_low.php');

	Page::modules('site_configuration', 'floorplan', 'floorplan_data', 'available_apartment', 'lease_term', 'site_plan', 'lead_source');

	// -- Retrieve integration being used for Check Availability Table
	$site_configuration->reset();
	$oSiteConfig = $site_configuration->getItem();
	if ($oSiteConfig->get('type') == 'resman' || $oSiteConfig->get('type') == 'realpage' || $oSiteConfig->get('type') == 'psi') {
	  $sApiSettings = 'settings_'.$oSiteConfig->get('type');
	  Page::modules($sApiSettings);

	  // -- Should always be 1 record in this module
	  $$sApiSettings->reset();
	  if ($$sApiSettings->hasItems()){
	    $oApiSettingsItem = $$sApiSettings->getItem();
	  }
	}

	// -- Retrieve item index to pre-select Schedule a Tour Form option
	$floorplan->reset();
	$aFloorplans = array();
	$iSelectedIndex = -1;
	foreach ($floorplan->getItems() as $iIndex => $fp_item){
	  $aFloorplans[] = $fp_item;
	  if ($fp_item->get('slug') == getParam(0)){
	    $iSelectedIndex = $iIndex;
	  }
	}

	// -- Floorplan
	$floorplan->reset();
	$floorplan->filter('slug', getParam(0));
	if ($floorplan->hasItems()) {
		$oFloorplan = $floorplan->getItem();

		// -- Determine Sold Out status
	  $bSoldOut = $oFloorplan->get('sold_out') == 'Yes' && $oSiteConfig->get('sold_out_text');

	  // -- Determine Floors
	  $multiple_floors_2d = $oFloorplan->get('second_floor_2d') || $oFloorplan->get('third_floor_2d');
	  $multiple_floors_3d = $oFloorplan->get('second_floor_3d') || $oFloorplan->get('third_floor_3d');

		// -- Determine Bedroom Verbiage
		$sBedroom = $oFloorplan->get('bedrooms');
		$sBedroom .= is_numeric($sBedroom) ? ' bed' : null;
	} else {
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: /floorplans/");
	}

	// -- Floorplan Data / Available Apartment
  $available_apartment->reset();
  $floorplan_data->reset();
  $floorplan_data->filter('floorplan_group_id', $oFloorplan->get('floorplan_group_id'));

  if ($oFloorplanData = $floorplan_data->getItem()) {
  	$available_apartment->filter('floorplan', $oFloorplanData->get('id_value'));
  	$mAvailableUnits = $available_apartment->hasItems();
	} else {
		$mAvailableUnits = false;
	}

	$aRentRange = get_rent_high_low($available_apartment, $floorplan_data, $oSiteConfig);

	// -- SEO
	$page_name = 'FLOORPLANS';
	require_once('includes/seo-setup.php');
	Page::title($oFloorplan->get('title') . ' | ' . $page_title);
	Page::description($page_description);
	Page::keywords($page_keywords);
	Page::robots($page_robots);

	// -- Page Banner
	$aPageBannerSettings = array(
		'back_button' => array(
			'title' => '« Back to Floorplans',
			'url' => '/floorplans/'
		)
	);
	
	// -- Header
	require_once('includes/header.php');

	// -- Page Scripts
	Page::addScript('floorplan.js');
?>

<div class="page">
	<?php require_once('includes/partials/page-banner.php'); ?>

	<div class="floorplan-details">
		<div class="floorplan-details__container">
			<div class="floorplan-details__image-pane">
	      <?php if ($oFloorplan->get('first_floor_2d') && $oFloorplan->get('first_floor_3d')): ?>
	        <ul class="floorplan-details__toggle floorplan-details__toggle--dimension">
	          <li class="floorplan-details__toggle-item color__primary--background">
	          	<a data-js-hook="dimension-toggle" href="#" id="2d" class="floorplan-details__toggle-link floorplan-details__toggle-link--active">2D</a>
	        	</li>
	          <li class="floorplan-details__toggle-item color__primary--background">
	          	<a data-js-hook="dimension-toggle" href="#" id="3d" class="floorplan-details__toggle-link">3D</a>
	          </li>
	        </ul>
	      <?php endif; ?>

				<?php
				/*
				| ---------------------------------------------
				|	2d Images
				| ---------------------------------------------
				| Logic here determines the tempate based on
				| the existence of 2d images.
				*/
				?>
			  <?php if ($oFloorplan->get('first_floor_2d')): ?>
			    <div class="floorplan-details__floor-wrap fps_2d" id="2d-wrap">
			      <img id="2d-first-floor" class="floorplan-details__image floorplan-details__image--visible" src="<?php $oFloorplan->output('first_floor_2d'); ?>" alt="<?php $oFloorplan->output('title'); ?>" title="<?php $oFloorplan->output('title'); ?>" />
			      <?php if ($oFloorplan->get('second_floor_2d')): ?>
			        <img id="2d-second-floor" class="floorplan-details__image" src="<?php $oFloorplan->output('second_floor_2d'); ?>" alt="<?php $oFloorplan->output('title'); ?>" title="<?php $oFloorplan->output('title'); ?>" />
			      <?php endif; ?>
			      <?php if ($oFloorplan->get('third_floor_2d')): ?>
			        <img id="2d-third-floor" class="floorplan-details__image" src="<?php $oFloorplan->output('third_floor_2d'); ?>" alt="<?php $oFloorplan->output('title'); ?>" title="<?php $oFloorplan->output('title'); ?>" />
			      <?php endif; ?>
			    </div>

			    <?php if ($oFloorplan->get('second_floor_2d')): ?>
			      <ul class="floorplan-details__toggle floorplan-details__toggle--floors" id="2d-toggle">
			        <li class="floorplan-details__toggle-item color__primary--background">
			        	<a data-js-hook="floor-toggle" href="#" class="floorplan-details__toggle-link floorplan-details__toggle-link--active" id="2d-first">Floor 1</a>
			        </li>
			        <li class="floorplan-details__toggle-item color__primary--background">
			        	<a data-js-hook="floor-toggle" class="floorplan-details__toggle-link" href="#" id="2d-second">Floor 2</a>
			        </li>
			        <?php if ($oFloorplan->get('third_floor_2d')): ?>
			          <li class="floorplan-details__toggle-item color__primary--background">
			          	<a data-js-hook="floor-toggle" class="floorplan-details__toggle-link" href="#" id="2d-third">Floor 3</a>
			          </li>
			        <?php endif; ?>
			      </ul>
			    <?php endif; ?>
			  <?php endif; ?>

				<?php
				/*
				| ---------------------------------------------
				|	3d Images
				| ---------------------------------------------
				| Logic here determines the tempate based on
				| the existence of 3d images.
				*/
				?>
			  <?php if ($oFloorplan->get('first_floor_3d')): ?>
			    <div class="floorplan-details__floor-wrap fps_3d" id="3d-wrap" style="display:none">
			      <img id="3d-first-floor" class="floorplan-details__image" src="<?php $oFloorplan->output('first_floor_3d'); ?>" alt="<?php $oFloorplan->output('title'); ?>" title="<?php $oFloorplan->output('title'); ?>" />
			      <?php if ($oFloorplan->get('second_floor_3d')): ?>
			        <img id="3d-second-floor" class="floorplan-details__image" src="<?php $oFloorplan->output('second_floor_3d'); ?>" alt="<?php $oFloorplan->output('title'); ?>" title="<?php $oFloorplan->output('title'); ?>" />
			      <?php endif; ?>
			      <?php if ($oFloorplan->get('third_floor_3d')): ?>
			        <img id="3d-third-floor" class="floorplan-details__image" src="<?php $oFloorplan->output('third_floor_3d'); ?>" alt="<?php $oFloorplan->output('title'); ?>" title="<?php $oFloorplan->output('title'); ?>" />
			      <?php endif; ?>
			    </div>

			    <?php if ($oFloorplan->get('second_floor_3d')): ?>
			     	<ul class="floorplan-details__toggle floorplan-details__toggle--floors" id="3d-toggle" style="display:none;">
			        <li class="floorplan-details__toggle-item color__primary--background">
			        	<a data-js-hook="floor-toggle" href="#" class="floorplan-details__toggle-link floorplan-details__toggle-link--active" id="3d-first">Floor 1</a>
			        </li>
			        <li class="floorplan-details__toggle-item color__primary--background">
			        	<a data-js-hook="floor-toggle" class="floorplan-details__toggle-link" href="#" id="3d-second">Floor 2</a>
			        </li>
			        <?php if ($oFloorplan->get('third_floor_3d')): ?>
			          <li class="floorplan-details__toggle-item color__primary--background">
			          	<a data-js-hook="floor-toggle" class="floorplan-details__toggle-link" href="#" id="3d-third">Floor 3</a>
			          </li>
			        <?php endif; ?>
			      </ul>
			    <?php endif; ?>

			  <?php endif; ?>
			</div>

			<div class="floorplan-details__title-table">
				<div class="floorplan-details__title-column floorplan-details__title-column--left">
					<p class="floorplan-details__title"><?php $oFloorplan->output('title'); ?></p>
					<?php if ($oSiteConfig->get('show_pricing_ranges') && $aRentRange['low'] && $aRentRange['high']): ?>
						<p class="floorplan-details__price">
							<?php  
								echo $aRentRange['low'];
								echo $aRentRange['low'] !== $aRentRange['high'] ? ' - ' . $aRentRange['high'] : null;
							?>
						</p>
					<?php endif; ?>
				</div>
				<div class="floorplan-details__title-column floorplan-details__title-column--right">
					<p class="floorplan-details__info"><?php echo $sBedroom . ' | ' . $oFloorplan->get('bathrooms')  . ' bath | ' . $oFloorplan->get('square_feet') . ' sq. ft.'; ?></p>
				</div>
			</div>

			<div class="floorplan-details__details">
				<?php if ($oFloorplan->get('description')): ?>
		      <div class="floorplan-details__description">
		      	<div class="page__content page__content--align-center">
			        <?php $oFloorplan->output('description'); ?>
		        </div>
		      </div>
		    <?php endif; ?>

	      <ul class="floorplan-details__button-wrap">
      		<?php if ($oFloorplan->get('pdf_download')): ?>
	          <li class="floorplan-details__button-wrap-item">
	          	<a class="floorplan-details__button" target="_blank" href="<?php echo $oFloorplan->output('pdf_download'); ?>">
								View PDF
							</a>
	          </li>
          <?php endif; ?>

          <?php if ($oSiteConfig->get('schedule_a_tour')): ?>
		        <li class="floorplan-details__button-wrap-item">
		        	<a class="floorplan-details__button" target="_blank" href="" id="schedule-tour-toggle">
								Schedule a Tour
							</a>
		        </li>
          <?php endif; ?>

					<?php if ($oSiteConfig->get('type') !== 'off' && $oSiteConfig->get('type') !== 'iframe' && $mAvailableUnits): ?>
	          <li class="floorplan-details__button-wrap-item">
					  	<a class="floorplan-details__button" target="_blank" href="#" id="check-availability">
								Check Availability
							</a>
					  </li>
	        <?php elseif ($oFloorplan->get('pricing_availability_link')): ?>
	          <li class="floorplan-details__button-wrap-item">
					  	<a class="floorplan-details__button" target="_blank" href="<?php $oFloorplan->output('pricing_availability_link'); ?>" >
								Check Availability
							</a>
					  </li>
	        <?php elseif ($oSiteConfig->get('type') == 'iframe' && $oCheckAvailability->get('iframe_code')): ?>
	          <li class="floorplan-details__button-wrap-item">
					  	<a class="floorplan-details__button" href="/check-availability" target="_self">
								Check Availability
							</a>
					  </li>
	        <?php elseif ($oSiteConfig->get('type') == 'off' && $oSiteConfig->get('lease_link') || $oSiteConfig->get('type') == 'iframe' && !$oCheckAvailability->get('iframe_code') && $oSiteConfig->get('lease_link')): ?>
						<li class="floorplan-details__button-wrap-item">
							<a class="floorplan-details__button" href="<?php $oSiteConfig->output('lease_link'); ?>" target="_blank">
								Lease Now
							</a>
						</li>
	        <?php endif; ?>
	    	</ul>

    		<?php if ($oSiteConfig->get('type') !== 'off' && $oSiteConfig->get('type') !== 'iframe' && $mAvailableUnits): ?>
					<?php require_once('includes/partials/check-availability-table.php'); ?>
    		<?php endif; ?>

		    <div class="floorplan-details__schedule-tour" id="schedule-tour-wrap">
			    <div class="floorplan-details__form" id="schedule-tour-form-wrap">
			    <?php require_once('includes/form/schedule-a-tour.php'); ?>
			    </div>
			  </div>
			</div>
		</div>
		<?php if ($oPageItem->get('disclaimer')): ?>
			<div class="page__disclaimer">
				<p class="page__disclaimer-text"><?php $oPageItem->output('disclaimer'); ?></p>
			</div>
		<?php endif; ?>
	</div>
</div>

<?php require_once('includes/footer.php'); ?>