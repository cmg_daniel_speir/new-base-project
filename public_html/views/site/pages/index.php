<?php  
	Page::modules('homepage_rotator', 'small_homepage_ad', 'homepage_content');

	// -- Rotator
	$homepage_rotator->reset();

	// -- Callouts
	$small_homepage_ad->reset();
	$small_homepage_ad->limit(4);

	// -- SEO
	$page_name = 'HOME_PAGE';
	require_once('includes/seo-setup.php');
	Page::title($page_title);
	Page::description($page_description);
	Page::keywords($page_keywords);
	Page::robots($page_robots);

	require_once('includes/header.php'); 
	
	Page::addScript('vendors/flexslider.js');
	Page::addScript('index.js');
?>

<div class="flexslider rotator" id="rotator">
	<ul class="slides">
		<?php foreach ($homepage_rotator->getItems() as $oAd): ?>
			<li>
				<img src="<?php $oAd->output('asset'); ?>" title="<?php $oAd->output('title'); ?>" alt="<?php $oAd->output('title'); ?>" />
				<?php if ($sOverlayTitle = $oAd->get('overlay_title')): ?>
					<p class="rotator__title">
						<?php echo preg_replace("/\*(.*)\*/", '<span>$1</span>', $sOverlayTitle); ?>
					</p>
				<?php endif; ?>
			</li>
		<?php endforeach; ?>
	</ul>
</div>

<?php require_once('includes/footer.php'); ?>