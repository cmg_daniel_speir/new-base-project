<?php 
	$sPageModule = 'schedule_a_tour_page';

	Page::modules($sPageModule);

	$$sPageModule->reset();
	if (!$oPageItem = $$sPageModule->getItem()) {
		die('Page not found.');
	}	

	// -- SEO
	$page_name = 'SCHEDULE_A_TOUR';
	require_once('includes/seo-setup.php');
	Page::title($page_title);
	Page::description($page_description);
	Page::keywords($page_keywords);
	Page::robots($page_robots);

	require_once('includes/header.php'); 
?>

<div class="page">
	<?php require_once('includes/partials/page-banner.php'); ?>

	<div class="page__body">
		<div class="page__container">
			<?php require_once('includes/form/schedule-a-tour.php'); ?>
		</div>
	</div>
</div>

<?php require_once('includes/footer.php'); ?>