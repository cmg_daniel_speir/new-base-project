<?php
// SETUP MODULES
Page::modules('ebrochure');

// Variables needed for str replacement
require_once('includes/ebrochure/settings.php');
require_once('includes/ebrochure/functions.php');

$download_id = (int)$_POST['download_id'];
$download_file = $doc_root . 'pdfs/ebrochure' . $download_id . '.pdf';
$download_link = $server_name . '/pdfs/ebrochure' . $download_id . '.pdf';

if (!isset($download_id) || !file_exists($download_file))
{
  header("location: /floorplans/ebrochure/");
  exit;
}

if ($ebrochure->hasItems())
{
  $ebrochure_item = $ebrochure->getItem();
}

$header_image = $ebrochure_item->get('email_header');
$text_color = $ebrochure_item->get('text_color');
$font_type = $ebrochure_item->get('font_type');
$from_email = $ebrochure_item->get('from_email');
$to_email = $_POST['brochureEmail'];
send_ebrochure_email($to_email,$download_link,$from_email,$font_type,$text_color,$header_image);
header("location: /floorplans/ebrochure/download/?download=$download_id&sent=1");