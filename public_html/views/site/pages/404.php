<?php
// Define Page Module
$sPageModule = 'four_oh_four_page';

// -- Module init
Page::modules($sPageModule);

// -- Page Item
$$sPageModule->reset();
$oPageItem = $$sPageModule->getItem();

// -- SEO
$page_name = 'PAGE_NOT_FOUND';
require_once('includes/seo-setup.php');
Page::title($page_title);
Page::description($page_description);
Page::keywords($page_keywords);
Page::robots($page_robots);

// -- Header
require_once('includes/header.php');

$bIs404 = true;
?>

<div class="page">
	<?php require_once('includes/partials/page-banner.php'); ?>
	<div class="page__container">
		<div class="page__content page__content--align-center page__content--space-bottom">
			<h5><?php $oPageItem->output('header'); ?></h5>
			<?php $oPageItem->output('body'); ?>
		</div>
		<?php require_once('includes/partials/site-map.php'); ?>
	</div>
</div>

<?php require_once('includes/footer.php'); ?>