<?php
// Variables needed for str replacement
require_once('includes/ebrochure/pdfmerger/PDFMerger.php');
require_once('includes/ebrochure/settings.php');

// SETUP MODULES
Page::modules('floorplan','ebrochure');

$floorplan->reset();
$ebrochure->reset();

if ($ebrochure->hasItems()) {
  $ebrochure_item = $ebrochure->getItem();
} else {
  header("location: /floorplans/ebrochure/");
}

if (!file_exists($doc_root . 'pdfs')) {
  mkdir($doc_root . 'pdfs');
}

$pdf = new PDFMerger;

$pdf->addPDF(str_replace($server_name . '/', $doc_root, $ebrochure_item->get('cover_pdf')), 'all');

if ($floorplan->hasItems()) {
  foreach ($floorplan->getItems() as $floorplan_item) {
    $fpid = $floorplan_item->get('id');
    if (isset($_POST["fp$fpid"])) {
      // If the file exists then add it to the stack to merge
      if (file_exists(str_replace($server_name, $doc_root, $floorplan_item->get('pdf_download')))) {
        $pdf->addPDF(str_replace($server_name, $doc_root, $floorplan_item->get('pdf_download')), 'all');
      }
    }
  }
}

$pdf->addPDF(str_replace($server_name, $doc_root, $ebrochure_item->get('footer_pdf')), 'all');
$file_date = date('ymdhis');
$final_file_name = 'ebrochure' . $file_date . '.pdf';
$pdf->merge('file', $doc_root . "pdfs/$final_file_name");

header("location: /floorplans/ebrochure/download/?download=$file_date");