<?php

if (!isAjax()) {
  require('404.php');
  exit;
}

Page::modules('social_network_tweet', 'short_url', 'email_share');

//Get POST data
$sTweetFilter = !empty($_POST['tweet_filter']) ? $_POST['tweet_filter'] : 'GLOBAL';
$sUrl = !empty($_POST['url']) ? $_POST['url'] : $_SERVER['HTTP_REFERER'];

//Get tweet text
$social_network_tweet->reset();
$social_network_tweet->filter('system_name', $sTweetFilter);
if ($social_network_tweet->hasItems()) {
  $sn_item = $social_network_tweet->getItem();
  $sTweet = $sn_item->get('tweet');
}
//Get a short URL for the referring page
$sShortUrl = $short_url->get();
$sTweet = str_replace('{SHORT_URL_VARIABLE}', $sShortUrl, $sTweet);
$sTweet = str_replace('{COMPANY_NAME}', Page::setting('company_name'), $sTweet);
$sTweet = str_replace(' ', '+', $sTweet);

//Get email details
$email_share->reset();
if ($email_share->hasItems()){
  $email_share_item = $email_share->getItem();
  $sEmailSubject = $email_share_item->get('email_subject');
  $sEmailBody = $email_share_item->get('email_body');
  $sEmailSubject  = str_replace(' ', '%20', $sEmailSubject);
  $sEmailSubject  = str_replace('{COMPANY_NAME}', Page::setting('company_name'), $sEmailSubject);
  $sEmailBody  = str_replace(' ', '%20', $sEmailBody);
  $sEmailBody  = str_replace('{COMPANY_NAME}', Page::setting('company_name'), $sEmailBody);
}
?>

<ul class="share-this-popup__list">
  <li class="share-this-popup__item">
    <a class="share-this-popup__link share-this-popup__link--twitter" href="http://twitter.com/home?status=<?php echo $sTweet; ?>" target="_blank" rel="nofollow">Twitter</a>
  </li>
  <li class="share-this-popup__item">
    <a class="share-this-popup__link share-this-popup__link--facebook" href="http://facebook.com/sharer.php?u=<?php echo $sShortUrl; ?>" target="_blank" rel="nofollow">Facebook</a>
  </li>
  <li class="share-this-popup__item">
    <a class="share-this-popup__link share-this-popup__link--google" href="https://plus.google.com/share?url=<?php echo urlencode($sUrl); ?>" target="_blank" rel="nofollow">Google+</a>
  </li>
  <li class="share-this-popup__item">
    <a class="share-this-popup__link share-this-popup__link--email" href="mailto:?Subject=<?php echo $sEmailSubject; ?>&Body=<?php echo $sEmailBody . ' ' . $sShortUrl; ?>" rel="nofollow">Email</a>
  </li>
</ul>