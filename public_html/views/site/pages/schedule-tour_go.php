<?php
Page::modules('form', 'site_configuration', 'api_leads');

$oSiteConfigItem = $site_configuration->getItem();
if (!is_object($oSiteConfigItem)) {
  respondDie('Site Configuration is not setup yet.');
}

$sApiIntegrationType = $oSiteConfigItem->get('type'); //will be one of (off, resman, realpage or psi)
$aApis = array('resman', 'realpage', 'psi');
if (in_array($sApiIntegrationType, $aApis)) {
  $sApiSettingsModuleName = 'settings_' . $sApiIntegrationType;

  //Include the CMS module for the specific property integration settings
  Page::modules($sApiSettingsModuleName);

  //ensure we have that module and record is set
  $oApiSettingsItem = $$sApiSettingsModuleName->getItem();
  if (!is_object($oApiSettingsItem)) {
    respondDie('Property integration settings are not setup yet.');
  }
}

//Honeypot.
//If the hidden email field is filled in then we know it was a bot that
//filled out the form, so we'll just return succuss without doing anything.
if (!empty($_POST['Email_Again'])) {
  respondDie('success');
}

//Insert data into CMS first.
//Then if API fails we at least have the fallback with the data stored in CMS form results.
if ($form->insert(21, $_POST, $_FILES)) {
  //If Property Integration is on (set to Resman, RealPage or PSI) then insertProspect
  //using API calls to store data through the API
  if (in_array($sApiIntegrationType, $aApis) && $oSiteConfigItem->get('post_leads') == 'Yes') {
    //Save the form in API Leads module - this is where we queue them up and keep track of the status of posting to the API
    //so we can retry them automatically later if they fail
    $iLeadApiId = insertApiLead($_POST);
    if ($iLeadApiId <= 0) {
      respondDie('Your request cannot be processed at this time. Please try again later.');
    }
    $_POST['lead'] = $iLeadApiId; //set this so the postResman function has access to it

    switch ($sApiIntegrationType) {
      case 'resman':
        $bResponse = postResman($oApiSettingsItem, $_POST);
        break;
      
      case 'realpage':
        $bResponse = postRealPage($oApiSettingsItem, $_POST);
        break;

      case 'psi':
        $bResponse = postPsi($oApiSettingsItem, $_POST);
        break;
      
      default:
        $bResponse = false;
        break;
    }

    //Store the lead in CMS module that is used simply for queueing up and storing the status of the API post
    //that way a cron job can retry posting the lead to the API if the initial post fails
    $sResponse = ($bResponse == false) ? 'failure' : 'success';
    $mResponse = updateApiLead($iLeadApiId, $sResponse);
    
    respondDie('success');
  }

  //No API integration is enabled - so we're all done successfully
  respondDie('success');
}
else {
  //form insert failed, log the errors
  foreach ($form->getError() as $sError) {
    $errors = $errors . $sError;
  }
  respondDie($errors);
}


/**
 * Posts the form data to the Resman API to store the prospect/guest card on the back end.
 * 
 * @param object $oApiSettingsItem
 * @param array  $aPostData
 * @return mResponse   Response from the API's postProspect method.
 */
function postResman($oApiSettingsItem, $aPostData) {
  $iIntegrationPartnerId = '1055';
  $sApiKey = '797e094763b84fb4aeac7b876a24c7f1';

  //ensure we have a property_id defined for our call to the API
  $sPropertyId = $oApiSettingsItem->get('property_id');
  if (empty($sPropertyId)) {
    respondDie('The API is currently not available.');
  }

  //floorplan contains the "{floorplan name}|{floorplan id_value}"
  $aFloorplanData = explode('|', $aPostData['Floorplan']);

  //submit to ResMan for a contact card to be created
  $aData = array();
  $aData[ResmanApi::DATA_FIRST_NAME] = $aPostData['FirstName']; //required
  $aData[ResmanApi::DATA_LAST_NAME] = $aPostData['LastName'];   //required
  $aData[ResmanApi::DATA_EMAIL] = $aPostData['Email'];          //required
  $aData[ResmanApi::DATA_PHONE_TYPE] = 'cell';                   //optional
  $aData[ResmanApi::DATA_PHONE] = $aPostData['Phone'];               //optional
  $aData[ResmanApi::DATA_APT_FLOORPLAN] = $aFloorplanData[1];        //optional
  $aData[ResmanApi::DATA_MOVE_IN_DATE] = $aPostData['MoveInDate'];   //optional - date in YYYY-MM-DD format
  $aData[ResmanApi::DATA_COMMENTS] = $aPostData['CommentsQuestions'];//optional
  
  $bLog = strtolower($oApiSettingsItem->get('log_api_requests')) == 'on' ? true : false;
  $oApi = new ResmanApi($bLog, Config::$SITE_URL);
  
  //$mResponse will be FALSE if the API call fails, otherwise an array of data that was the reponse from the API
  $mResponse = $oApi->postProspect($iIntegrationPartnerId, $sApiKey, $oApiSettingsItem->get('property_id'), $aData);
  if (is_array($mResponse)) {
    //successful API response will be an array that's the original data posted through the API
    return true;
  }
  else {
    return false;
  }
}

/**
 * Post the prospet to the RealPage API.
 * 
 * @param type $oApiSettingsItem
 * @param type $_POST
 * @return boolean
 */
function postRealPage($oApiSettingsItem, $aPostData) {
  $sPmcId = $oApiSettingsItem->get('pmc_id');
  $sSiteId = $oApiSettingsItem->get('site_id');
  $sUsername = $oApiSettingsItem->get('username');
  $sPassword = $oApiSettingsItem->get('password');
  if (empty($sPmcId) || empty($sSiteId) || empty($sUsername) || empty($sPassword)) {
    respondDie('The API settings must be defined.');
  }
  
  //floorplan contains the "{floorplan name}|{floorplan id_value}"
  $aFloorplanData = explode('|', $aPostData['Floorplan']);

  $aData = array();
  $aData[RealPageApi::DATA_NAME] = $aPostData['FirstName'] . ' ' . $aPostData['LastName'];
  $aData[RealPageApi::DATA_EMAIL] = $aPostData['Email'];
  $aData[RealPageApi::DATA_PHONE] = $aPostData['Phone'];
  $aData[RealPageApi::DATA_PHONE_TYPE] = 'Mobile'; //picklist options from uptownsanmarcos.com: HO-Home, WO-Work, MO-Mobile, PA-Pager, FH-Fax Home, FW-Fax Work, OT-Other
  $aData[RealPageApi::DATA_CONTACT_TYPE] = '';
  $aData[RealPageApi::DATA_LEAD_SOURCE] = isset($aPostData['LeadSource']) ? $aPostData['LeadSource'] : '';

  //DateNeeded, Occupants, and LeaseTermMonths are all required IF you're going to submit these 5 fields to
  //the API. Otherwise the fields are just ignored in our class and not sent to the API.
  $aData[RealPageApi::DATA_DATE_NEEDED] = $aPostData['MoveInDate'];;
  $aData[RealPageApi::DATA_OCCUPANTS] = '';
  $aData[RealPageApi::DATA_LEASE_TERM_MONTHS] = '';
  $aData[RealPageApi::DATA_PRICE_RANGE_ID] = '';
  $aData[RealPageApi::DATA_FLOORPLAN_GROUP_ID] = '';
  $aData[RealPageApi::DATA_FLOORPLAN_ID] = $aFloorplanData[1];
  
  $aData[RealPageApi::DATA_COMMENTS] = $aPostData['CommentsQuestions'];
  
  $bLog = strtolower($oApiSettingsItem->get('log_api_requests')) == 'on' ? true : false;
  $oApi = new RealPageApi($sUsername, $sPassword, $sPmcId, $sSiteId, $bLog, Config::$SITE_URL);
  $bResponse = $oApi->postProspect($sPmcId, $sSiteId, $sUsername, $sPassword, $aData);
  if ($bResponse) {
    //successful API response will be an array that's the original data posted through the API
    return true;
  }
  else {
    return false;
  }
}

/**
 * Submits a MitsLead through the PSI API.
 *
 * @param type $oApiSettingsItem
 * @param type $aPostData
 * @return boolean
 */
function postPsi($oApiSettingsItem, $aPostData) {
  $sUsername = $oApiSettingsItem->get('username');
  $sPassword = $oApiSettingsItem->get('password');
  $sApiSubdomain = $oApiSettingsItem->get('api_subdomain');
  $sPropertyId = $oApiSettingsItem->get('property_id');
  if (empty($sUsername) || empty($sPassword) || empty($sApiSubdomain) || empty($sPropertyId)) {
    respondDie('The API settings must be defined.');
  }
  
  //floorplan contains the "{floorplan name}|{floorplan id_value}"
  $aFloorplanData = explode('|', $aPostData['Floorplan']);

  $aData = array();
  $aData[PropertySolutionsApi::DATA_FIRST_NAME] = $aPostData['FirstName'];
  $aData[PropertySolutionsApi::DATA_LAST_NAME] = $aPostData['LastName'];
  $aData[PropertySolutionsApi::DATA_MIDDLE_NAME] = '';
  $aData[PropertySolutionsApi::DATA_EMAIL] = $aPostData['Email'];
  $aData[PropertySolutionsApi::DATA_PHONE] = $aPostData['Phone'];
  $aData[PropertySolutionsApi::DATA_PHONE_TYPE] = 'cell';
  $aData[PropertySolutionsApi::DATA_ORIGINATING_LEAD_SOURCE] = !empty($aPostData['LeadSource']) ? $aPostData['LeadSource'] : '';
  $aData[PropertySolutionsApi::DATA_APT_FLOORPLAN] = $aFloorplanData[1];
  $aData[PropertySolutionsApi::DATA_COMMENTS] = $aPostData['CommentsQuestions'];
  $aData[PropertySolutionsApi::DATA_MOVE_IN_DATE] = $aPostData['MoveInDate'];
  
  $aData[PropertySolutionsApi::DATA_APT_DESIRED_BEDROOMS] = '';
  $aData[PropertySolutionsApi::DATA_APT_DESIRED_LEASE_TERM] = '';
  $aData[PropertySolutionsApi::DATA_APT_DESIRED_RENT] = '';
  $aData[PropertySolutionsApi::DATA_APT_IDVALUE] = '';
  $aData[PropertySolutionsApi::DATA_APT_NUMBER] = '';
  
  $bLog = strtolower($oApiSettingsItem->get('log_api_requests')) == 'on' ? true : false;
  $oApi = new PropertySolutionsApi($sUsername, $sPassword, $sPropertyId, $sApiSubdomain, $bLog, Config::$SITE_URL);
  
  //$mResponse will be FALSE if the API call fails, otherwise an array of data that was the reponse from the API
  $mResponse = $oApi->postProspect($aData);
  if (is_array($mResponse)) {
    //successful API response will be an array that's the original data posted through the API
    return true;
  }
  else {
    return false;
  }
}

/**
 * Save a brand new ApiLeads module record and return back the new CMS record's ID value.
 *
 * @return int   id of the new CMS record
 */
function insertApiLead() {
  $oModule = getModuleCrud('api_leads', false);
  $aApiLead = array();
  $aApiLead['modify_date'] = date('Y-m-d H:i:s');
  $aApiLead['create_date'] = date('Y-m-d H:i:s');
  $aApiLead['form_data'] = json_encode($_POST);
  $aApiLead['status'] = 'In Progress';
  $aApiLead['last_attempt'] = date('Y-m-d H:i:s');
  $aApiLead['last_attempt_timezone'] = 'America/Chicago';
  $aApiLead['history'] = date('Y-m-d H:i:s') . " - created, first attempt\n";
  $oModule->createEntry($aApiLead);
  return $oModule->iId;
}

/**
 * Update the status for an ApiLead record in CMS
 * 
 * @param ing $iId
 * @param string $sStatus
 * @return boolean
 */
function updateApiLead($iId, $sStatus) {
  global $api_leads;
  
  $api_leads->reset();
  $api_leads->filter('id', $iId);
  if (!$api_leads->hasItems()) {
    return false;
  }
  $oLead = $api_leads->getItem();

  $oModule = getModuleCrud('api_leads', false);
  
  $aApiLead = array();
  $aApiLead['id'] = $iId;
  $aApiLead['modify_date'] = date('Y-m-d H:i:s');
  $aApiLead['create_date'] = $oLead->get('create_date');
  $aApiLead['form_data'] = $oLead->get('form_data');
  $aApiLead['status'] = ucwords($sStatus); // has to be "Success", "Failure", or "In Progress"
  $aApiLead['last_attempt']= $oLead->get('last_attempt');
  $aApiLead['last_attempt_timezone'] = $oLead->get('last_attempt_timezone');
  $aApiLead['history'] = $oLead->get('history') . date('Y-m-d H:i:s') . " - $sStatus\n";
  $response = $oModule->updateEntry($aApiLead);
  return $response;
}

/**
 * echoes out the JSON response and then EXITs the PHP script.
 * 
 * @param type $sResponse   Either "success" or an error string
 */
function respondDie($sResponse='success') {
  echo $sResponse;
  exit;
}
