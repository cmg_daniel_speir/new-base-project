<?php 
	$sPageModule = 'privacy_policy_page';

	Page::modules($sPageModule);

	$$sPageModule->reset();
	if (!$oPageItem = $$sPageModule->getItem()) {
		die('Page not found.');
	}	

	// -- SEO
	$page_name = 'PRIVACY_POLICY';
	require_once('includes/seo-setup.php');
	Page::title($page_title);
	Page::description($page_description);
	Page::keywords($page_keywords);
	Page::robots($page_robots);

	require_once('includes/header.php'); 
?>

<div class="page">
	<?php require_once('includes/partials/page-banner.php'); ?>
	<div class="page__container">
		<div class="page__content">
			<p><?php $oPageItem->output('content'); ?></p>
		</div>
	</div>
</div>

<?php require_once('includes/footer.php'); ?>