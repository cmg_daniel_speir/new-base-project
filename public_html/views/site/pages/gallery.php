<?php 
$sPageModule = 'gallery_page';

Page::modules($sPageModule, 'gallery_image');

// -- Page Item
$$sPageModule->reset();
if (!$oPageItem = $$sPageModule->getItem()) {
	die('Page not found.');
}	

// -- Gallery
$gallery_image->reset();

	// -- SEO
$page_name = 'GALLERY';
require_once('includes/seo-setup.php');
Page::title($page_title);
Page::description($page_description);
Page::keywords($page_keywords);
Page::robots($page_robots);

// -- Header
require_once('includes/header.php'); 

// -- View Scripts
Page::addScript('vendors/swipebox.js');
Page::addScript('gallery.js');
?>

<div class="page">
	<?php require_once('includes/partials/page-banner.php'); ?>
	<div class="page__container">
		<div class="gallery">
			<?php if ($gallery_image->hasItems()): ?>
			  <?php foreach ($gallery_image->getItems() as $photo): ?>
			    <a class="gallery__link" href="<?php $photo->output('photo') ?>" data-js-hook="swipebox" title="<?php $photo->output('title') ?>">
			    	<img class="gallery__image" src="<?php $photo->output('photo', 'width=310&height=170&crop=1') ?>" alt="<?php $photo->output('title') ?>" title="<?php $photo->output('title') ?>">
			    </a>
			  <?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php require_once('includes/footer.php'); ?>