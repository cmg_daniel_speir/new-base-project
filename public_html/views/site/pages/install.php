<?php
/* SAMPLE PHP CODE TO RE-CREATE THIS MODULE */
include_once '../../../../app/includes/churchmedia/util/loader.php';

//get this from the install
$aPostFields['ik'] = 'Pn3JP4Nq3CBjyfB2hSgdGgqJ3C4Pnj5H';
$aPostFields['api-category'] = 'module';
$aPostFields['api-action'] = 'add';

//Module Data
$aPostFields['aModuleData']['title'] = 'Ebrochure Page';
$aPostFields['aModuleData']['name'] = 'floorplans_ebrochure_page';
$aPostFields['aModuleData']['description'] = '';
$aPostFields['aModuleData']['rss_available'] = 'off';
$aPostFields['aModuleData']['module_is_active'] = 'on';
$aPostFields['aModuleData']['sort_column'] = 'id';
$aPostFields['aModuleData']['sort_order'] = 'DESC';
$aPostFields['aModuleData']['versioning_is_on'] = 0;

//TEXT - Display Name
$aComponent['type'] = 'text';
$aComponent['title'] = 'Display Name';
$aComponent['name'] = 'title';
$aComponent['caption'] = '';
$aComponent['active'] = 1;
$aComponent['options']['listPage'] = true;
$aComponent['options']['disableEdit'] = false;
$aComponent['options']['cmg_master'] = 0;
$aComponent['options']['slug'] = false;
$aComponent['options']['size'] = 'normal';
$aComponent['validation']['required'] = 1;

$aPostFields['aModuleData']['components'][] = $aComponent;
unset($aComponent);

//ASSET - Banner
$aComponent['type'] = 'asset';
$aComponent['title'] = 'Banner';
$aComponent['name'] = 'banner';
$aComponent['caption'] = '';
$aComponent['active'] = 1;
$aComponent['validation']['required'] = true;
$aComponent['options']['listPage'] = true;
$aComponent['options']['disableEdit'] = false;
$aComponent['options']['cmg_master'] = 0;
$aComponent['options']['type'] = array('image');
$aComponent['options']['width'] = '1800';
$aComponent['options']['height'] = '540';
$aComponent['options']['allowMultiple'] = 0;
$aPostFields['aModuleData']['components'][] = $aComponent;
unset($aComponent);

//JSON encode the array to pass to the url via curl
$jData['jsonDataAPI'] = json_encode($aPostFields);

//url to send data to
$sApiUrl = CONFIG::$SITE_URL . '/cms/api/';

//data from the curl request
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $sApiUrl);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 100);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $jData);
$mData = curl_exec($ch);
curl_close($ch);
print_r($mData);
?>