(function() {
  'use strict';

  // -- Utilities
  var gulp    = require('gulp');
  var plumber = require('gulp-plumber');
  var notify  = require('gulp-notify');

  // -- SASS/CSS
  var sass          = require('gulp-sass');
  var bourbon       = require('node-bourbon');
  var sourcemaps    = require('gulp-sourcemaps');
  var autoprefixer  = require('gulp-autoprefixer');

  // -- Paths
  var basePath = 'public_html/views/site/';
  var sassPath = basePath + 'scss/{,*/}*.scss';
  var cssPath  = basePath + 'css';

  // --------------------------------------------------------------------------
  //  Settings
  // --------------------------------------------------------------------------

  // -- Sass Options
  var sassOptions = {
    errLogToConsole: true,
    outputStyle: 'compressed',
    includePaths: bourbon.includePaths
  };

  // -- Autoprefixer Options
  var autoprefixerOptions = {
    browsers: ['last 5 versions']
  };

  // --------------------------------------------------------------------------
  //  Gulp Task: Styles
  // --------------------------------------------------------------------------
  gulp.task('styles', function() {
    return gulp
      .src(sassPath)
      .pipe(sourcemaps.init())
      .pipe(plumber({
        errorHandler: notify.onError({
          title: 'Gulp',
          message: '<%= error.message %>',
        })
      }))
      .pipe(sass(sassOptions).on('error', sass.logError))
      .pipe(autoprefixer(autoprefixerOptions))
      .pipe(sourcemaps.write('/'))
      .pipe(gulp.dest(cssPath))
      .pipe(notify({
           title: 'Gulp',
           message: 'Styles Completed'
       }))
      .resume();
  });


  // --------------------------------------------------------------------------
  //  Gulp Task: Watch
  // --------------------------------------------------------------------------
  gulp.task('watch', function() {
    return gulp
      .watch(sassPath, ['styles'])
      .on('change', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
      });
  });


  // --------------------------------------------------------------------------
  //  Gulp Task: Default
  // --------------------------------------------------------------------------
  gulp.task('default', ['watch']);
}());